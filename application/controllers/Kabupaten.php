<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kabupaten extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		//Load Dependencies
		$this->load->model('m_gallery');
        $this->load->model('m_perusahaan');
        $this->load->model('m_icon');
        $this->load->model('m_kabupaten');

	}

	// List all your items
	public function index()
	{
		$data = array(
			'title' => 'Data Kabupaten',
			'hitung'	=> $this->m_kabupaten->hitung(),
			'isi'	=> 'kabupaten/v_index'
		);
		$this->load->view('layout2/v_wrapper', $data, FALSE);
	}

        // List all your items
    public function detail($data)
    {
        $data = array(
            'title' => 'Detail Data Kabupaten',
            'detail'    => $this->m_kabupaten->detail($data),
            'hitung'	=> $this->m_kabupaten->hitung(),
            'isi'   => 'kabupaten/v_detail'
        );
        $this->load->view('layout2/v_wrapper', $data, FALSE);
    }

	// Add a edit item

    public function edit($id_kabupaten)
    {
        $this->form_validation->set_rules('n_kabupaten', 'Nama Kabupaten','trim');
        $this->form_validation->set_rules('kd_kabupaten', 'Kode Kabupaten','trim');
        $this->form_validation->set_rules('kode_warna', 'Kode Warna','trim');

        if ($this->form_validation->run() == FALSE) {
            $data = array(

                'title' => 'Edit Data Kabupaten',
                'kabupaten' => $this->m_kabupaten->detaixl($id_kabupaten),
                'isi' => 'kabupaten/v_edit',
            );
            $this->load->view('layout2/v_wrapper', $data, FALSE);
        } else {

            $data = array(
                'id_kabupaten' => $id_kabupaten,
                'kd_kabupaten' => $this->input->post('kd_kabupaten'),
                'n_kabupaten' => $this->input->post('n_kabupaten'),
                'kode_warna' => $this->input->post('kode_warna'),
                
            );
            $this->m_kabupaten->edit($data);
            $this->session->set_flashdata('pesan', 'Data sudah diedit');
            redirect('kabupaten');
        }
    }
    // Add a new item

        public function add()
    {
        $this->form_validation->set_rules('n_kabupaten', 'Nama Kabupaten', 'required', array('required' => '%s Harus Diisi!!'));
        $this->form_validation->set_rules('kd_kabupaten', 'Kode Kabupaten', 'required', array('required' => '%s Harus Diisi!!'));
        //$this->form_validation->set_rules('kode_warna', 'Kode Warna', 'required', array('required' => '%s Harus Diisi!!'));

         if ($this->form_validation->run() == TRUE) {
        //     $config['upload_path']          = './geojson/';
        //     $config['allowed_types']        = '*';
        //     $config['max_size']             = 2000;
        //     $this->upload->initialize($config);
        //    if (!$this->upload->do_upload('geojson_kabupaten')) {
             $data = array(
                 'title' => 'Input Data Kabupaten',
                 'error_upload' => $this->upload->display_errors(),
                 'isi' => 'kabupaten/v_add',
            );
             $this->load->view('layout2/v_wrapper', $data, FALSE);
        //     } else {
        //         $upload_data = array('uploads' => $this->upload->data());
        //         $config['image_library'] = 'gd2';
        //         $config['source_image'] = './geojson/' . $upload_data['uploads']['file_name'];
        //         $this->load->library('image_lib', $config);
                $data = array(
                    'n_kabupaten' => $this->input->post('n_kabupaten'),
                    'kd_kabupaten' => $this->input->post('kd_kabupaten'),
                   // 'kode_warna' => $this->input->post('kode_warna'),
                   // 'geojson_kabupaten' => $upload_data['uploads']['file_name'],
                );
                $this->m_kabupaten->add($data);
            $this->session->set_flashdata('pesan', 'Data Disimpan');
            redirect('kabupaten');
            }
        
        $data = array(
           
                'title' => 'Input Data Kabupaten',
                'isi' => 'kabupaten/v_add'
        );
        $this->load->view('layout2/v_wrapper', $data, FALSE);
    }


    public function delete($id_kabupaten)
    {
        $data = array('id_kabupaten' => $id_kabupaten);
        $this->m_kabupaten->delete($data);
        $this->session->set_flashdata('pesan', 'Data sudah dihapus');
        redirect('kabupaten');
    }
}
/* End of file Perusahaan.php */


/* End of file Controllername.php */
