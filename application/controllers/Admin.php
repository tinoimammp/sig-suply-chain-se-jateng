<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_perusahaan');
		$this->load->model('m_kabupaten');
		$this->load->model('m_icon');
		$this->load->model('m_berita');
	}

//Halaman Utama Administrator
	public function index()
	{
		$data = array(
			'title' => 'Modul Administrator',
			'title1' => 'Dashboard Administrator',
			'total'	=> $this->m_perusahaan->total(),
			'totalkabupaten'	=> $this->m_kabupaten->totalkabupaten(),
			'totalberita'	=> $this->m_berita->totalberita(),
			'totalkategori'	=> $this->m_icon->totalkategori(),
			'perusahaan' => $this->m_perusahaan->get_all_data(),
			'isi'	=> 'v_admin'
		);
		$this->load->view('layout2/v_wrapper', $data, FALSE);
	}

}
