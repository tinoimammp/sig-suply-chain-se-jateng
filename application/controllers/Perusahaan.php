<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Perusahaan extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		//Load Dependencies
		$this->load->model('m_perusahaan');
		$this->load->model('m_icon');
		$this->load->model('m_kabupaten');

	}

	// List all your items
	public function index()
	{
		$data = array(
			'title' => 'Data Perusahaan',
			'perusahaan'	=> $this->m_perusahaan->get_all_data(),
			'kabupaten'	=> $this->m_kabupaten->get_all_data(),
			'isi'	=> 'perusahaan/v_index'
		);
		$this->load->view('layout2/v_wrapper', $data, FALSE);
	}

	// Add a new item
	public function add()
	{
		$this->form_validation->set_rules('nama_tempat', 'Nama Tempat', 'required', array(
			'required' => '%s Harus Diisi !!!'
		));
		$this->form_validation->set_rules('alamat', 'alamat', 'required', array(
			'required' => '%s Harus Diisi !!!'
		));


		$this->form_validation->set_rules('id_kabupaten', 'Kabupaten', 'required', array(
			'required' => '%s Harus Diisi !!!'
		));

		$this->form_validation->set_rules('prov', 'Provinsi', 'required', array(
			'required' => '%s Harus Diisi !!!'
		));
		

		if ($this->form_validation->run() == TRUE) {
			$config['upload_path']          = './gambar/';
			$config['allowed_types']        = 'gif|jpg|png|jpeg';
			$config['max_size']             = 2000;
			$this->upload->initialize($config);

			if (!
				$this->upload->do_upload('gambar'))


			
			{
				$data = array(
					'title' => 'Input Data Perusahaan',
					'icon'	=> $this->m_icon->get_all_data(),
					'kabupaten'	=> $this->id_kabupaten->get_all_data(),
					'error_upload' => $this->upload->display_errors(),
					'isi'	=> 'perusahaan/v_add'
				);
				$this->load->view('layout2/v_wrapper', $data, FALSE);
			} 

			else {
				$upload_data = array('uploads' => $this->upload->data());
				$config['image_library'] = 'gd2';
				$config['source_image'] = './gambar/' . $upload_data['uploads']['file_name'];
				$this->load->library('image_lib', $config);
				$data = array(
					'nama_tempat' => $this->input->post('nama_tempat'),
					'alamat' => $this->input->post('alamat'),
					// 'desa' => $this->input->post('desa'),
					'id_kabupaten' => $this->input->post('id_kabupaten'),
					// 'kab' => $this->input->post('kab'),
					'prov' => $this->input->post('prov'),
					'telp' => $this->input->post('telp'),
					'emailp' => $this->input->post('emailp'),
					'latitude' => $this->input->post('latitude'),
					'longitude' => $this->input->post('longitude'),
					'id_icon' => $this->input->post('id_icon'),
					'gambar' => $upload_data['uploads']['file_name'],

				);
				$this->m_perusahaan->add($data);
				$this->session->set_flashdata('pesan', 'Data Berhasil Disimpan !!!');
				redirect('perusahaan');
			}
		}
		$data = array(
			'title' => 'Input Data Perusahaan',
			'icon'	=> $this->m_icon->get_all_data(),
			'kabupaten'	=> $this->m_kabupaten->get_all_data(),
			'isi'	=> 'perusahaan/v_add'
		);
		$this->load->view('layout2/v_wrapper', $data, FALSE);
	}

	public function edit($id_perusahaan)
	{
		$this->form_validation->set_rules('nama_tempat', 'Nama Tempat', 'required', array(
			'required' => '%s Harus Diisi !!!'
		));
		$this->form_validation->set_rules('alamat', 'alamat', 'required', array(
			'required' => '%s Harus Diisi !!!'
		));

		$this->form_validation->set_rules('id_kabupaten', 'Kabupaten', 'required', array(
			'required' => '%s Harus Diisi !!!'
		));

		$this->form_validation->set_rules('prov', 'Provinsi', 'required', array(
			'required' => '%s Harus Diisi !!!'
		));

		if ($this->form_validation->run() == TRUE) {
			$config['upload_path']          = './gambar/';
			$config['allowed_types']        = 'gif|jpg|png|jpeg';
			$config['max_size']             = 2000;
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('gambar')) {
				$data = array(
					'title' => 'Edit Data Perusahaan',
					'icon'	=> $this->m_icon->get_all_data(),
					'kabupaten'	=> $this->m_kabupaten->get_all_data(),

					'perusahaan' => $this->m_perusahaan->detail($id_perusahaan),
					'error_upload' => $this->upload->display_errors(),
					'isi'	=> 'perusahaan/v_edit'
				);
				$this->load->view('layout2/v_wrapper', $data, FALSE);
			} else {
				// jika ada pergantian diganti
				$upload_data = array('uploads' => $this->upload->data());
				$config['image_library'] = 'gd2';
				$config['source_image'] = './gambar/' . $upload_data['uploads']['file_name'];
				$this->load->library('image_lib', $config);
				$data = array(
					'id_perusahaan'	=> $id_perusahaan,
					'nama_tempat' => $this->input->post('nama_tempat'),
					'alamat' => $this->input->post('alamat'),
					'id_kabupaten' => $this->input->post('id_kabupaten'),
					'prov' => $this->input->post('prov'),
					'telp' => $this->input->post('telp'),
					'emailp' => $this->input->post('emailp'),
					'latitude' => $this->input->post('latitude'),
					'longitude' => $this->input->post('longitude'),
					'id_icon' => $this->input->post('id_icon'),
					'gambar' => $upload_data['uploads']['file_name'],
				);
				$this->m_perusahaan->edit($data);
				$this->session->set_flashdata('pesan', 'Data Berhasil Diedit !!!');
				redirect('perusahaan');
			}
			// jika foto tidak diganti
			$data = array(
				'id_perusahaan'	=> $id_perusahaan,
				'nama_tempat' => $this->input->post('nama_tempat'),
				'alamat' => $this->input->post('alamat'),
				'id_kabupaten' => $this->input->post('id_kabupaten'),
				'prov' => $this->input->post('prov'),
				'telp' => $this->input->post('telp'),
				'emailp' => $this->input->post('emailp'),
				'latitude' => $this->input->post('latitude'),
				'longitude' => $this->input->post('longitude'),
				'id_icon' => $this->input->post('id_icon'),
			);
			$this->m_perusahaan->edit($data);
			$this->session->set_flashdata('pesan', 'Data Berhasil Diedit !!!');
			redirect('perusahaan');
		}
		$data = array(
			'title' => 'Edit Data User',
			'perusahaan' => $this->m_perusahaan->detail($id_perusahaan),
			'kabupaten'	=> $this->m_kabupaten->get_all_data(),
			'icon'	=> $this->m_icon->get_all_data(),
			'isi'	=> 'perusahaan/v_edit'
		);
		$this->load->view('layout2/v_wrapper', $data, FALSE);
	}

	//Delete one item
	public function delete($id_perusahaan)
	{
		$data = array('id_perusahaan' => $id_perusahaan);
		$this->m_perusahaan->delete($data);
		$this->session->set_flashdata('pesan', 'Data Berhasil Dihapus !!!');
		redirect('perusahaan');
	}
}

/* End of file Controllername.php */
