<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('m_gallery');
		$this->load->model('m_perusahaan');
		$this->load->model('m_icon');
		$this->load->model('m_kabupaten');
	}

	public function index()
	{

		$gallery=$this->m_gallery->lists();

		$data = array(
			'title' => 'Data Foto perusahaan',
			'gallery'	=>	$gallery,
			'isi'	=> 'gallery/v_index',
		);
		$this->load->view('layout2/v_wrapper', $data, FALSE);
	}

	public function addfoto($id_perusahaan)
	{
		$this->form_validation->set_rules('ket_foto', 'Keterangan Foto','required',
        array('required' => '%s Harus Diisi'));

        if ($this->form_validation->run()) {
        	$config['upload_path']   = './gambar/foto_perusahaan/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size']      = 1500;
            $config['max_width']     = 5000;
            $config['max_height']    = 5000;
            $this->upload->initialize($config);
            if(! $this->upload->do_upload('foto_perusahaan')) {
					$perusahaan=$this->m_gallery->detail($id_perusahaan);
					$foto=$this->m_gallery->foto($id_perusahaan);
					$data = array(
									'title2' => 'Add Foto : '.$perusahaan->nama_tempat,
									'isi'	=> 'gallery/v_add',
									'error_upload' => $this->upload->display_errors(),
									'perusahaan'	=>	$perusahaan,
									'foto'	=>	$foto,
								);
            $this->load->view('layout2/v_wrapper', $data, FALSE);
			}else{
		$upload_data        		= array('uploads' =>$this->upload->data());
		$config['image_library']  	= 'gd2';
		$config['source_image']   	= './gambar/foto_perusahaan/'.$upload_data['uploads']['file_name'];

		$this->load->library('image_lib', $config);
				$this->image_lib->resize();
				$i = $this->input;
	            $data = array(
	            				'id_perusahaan'=> $id_perusahaan,
	            				'ket_foto'	=> $i->post('ket_foto'),
	                    		'foto_perusahaan'	=> $upload_data['uploads']['file_name'],
	                        	);
	            $this->m_gallery->addfoto($data);
	            $this->session->set_flashdata('sukses',' Data Kategori Berhasil Ditambahkan !');
	            redirect('gallery/addfoto/'.$id_perusahaan,'refresh');
		}
	}

		$perusahaan=$this->m_gallery->detail($id_perusahaan);
					$perusahaan=$this->m_gallery->detail($id_perusahaan);
					$foto=$this->m_gallery->foto($id_perusahaan);
					$data = array('title' => 'Tambah Foto perusahaan',
									'title2' => 'Add Foto : '.$perusahaan->nama_tempat,
			                        'isi'	=>	'gallery/v_add',			                     
									'perusahaan'	=>	$perusahaan,
									'foto'	=>	$foto,
								);
            $this->load->view('layout2/v_wrapper', $data, FALSE);

	}


	//Delete one item
	public function delete($id_perusahaan,$id_foto)
	{
		//hapus gambar
		$perusahaan=$this->m_gallery->detail($id_perusahaan);
		$foto=$this->m_gallery->detailfoto($id_foto);
		if ($foto->foto_perusahaan != "") {
			unlink('./gambar/foto_perusahaan/'.$foto->foto_perusahaan);
		}
		//===========================
		$data = array('id_foto' => $id_foto);
		$this->m_gallery->delete($data);
		$this->session->set_flashdata('sukses','Foto Berhasil Dihapus');
		redirect('gallery/addfoto/'.$id_perusahaan,'refresh');
	}

}

/* End of file Gallery.php */
/* Location: ./application/controllers/Gallery.php */