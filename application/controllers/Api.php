<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

	//memanggil model yang terkait
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_perusahaan');
		$this->load->model('m_kabupaten');
		$this->load->model('m_icon');
	}

//Mengambil data kabupaten dari database
	public function data($jenis='kabupaten',$type='point',$id='')
	{
		header('Content-Type: application/json');
		$response=[];
		if($jenis=='kabupaten'){
			$getKabupaten=$this->m_kabupaten->get();
			foreach ($getKabupaten->result() as $row) {
				$data=null;
				$data['id_kabupaten']=$row->id_kabupaten;
				$data['kd_kabupaten']=$row->kd_kabupaten;
				$data['geojson_kabupaten']=$row->geojson_kabupaten;
				$data['kode_warna']=$row->kode_warna;
				$data['n_kabupaten']=$row->n_kabupaten;
				$response[]=$data;
			}
			echo "var dataKabupaten=".json_encode($response,JSON_PRETTY_PRINT);
		}
//mengambil data icon atau kategori perusahaan  dari model M_icon
		if($jenis=='icon'){
			$getIcon=$this->m_icon->get();
			foreach ($getIcon->result() as $row) {
				$data=null;
				$data['id_icon']=$row->id_icon;
				$data['nama_icon']=$row->nama_icon;
				$data['icon']=($row->icon=='')?base_url('assets/marker/marker1.png'):base_url('assets/unggah/marker/'.$row->icon);
				$response[]=$data;
			}
			echo "var dataIcon=".json_encode($response,JSON_PRETTY_PRINT);
		}
//mengambil data perusahaan dari model m_perusahaan
		elseif($jenis=='perusahaan'){
			if($type=='point'){
				if($id!=''){
					$this->db->where('a.id_icon',$id);
				}
				$getPerusahaan=$this->m_perusahaan->get();
				foreach ($getPerusahaan->result() as $row) {
					$data=null;
					$data['type']="Feature";
					$data['properties']=[
												"name"=>$row->nama_tempat,
												"lokasi"=>$row->alamat.' Kab. '.$row->n_kabupaten,
												"link"=>$row->id_perusahaan,
												"a"=>[$row->latitude,$row->longitude ],
												"gambar"=>($row->gambar=='')?('gambar/'):('gambar/'.$row->gambar),
												"icon"=>($row->icon=='')?base_url('marker'):base_url('marker/'.$row->icon),
												"popUp"=> "<b>Nama :</b> ".$row->nama_tempat.
												"<br><b>Kabupaten : </b> ".$row->n_kabupaten."<br><b>Jenis Perusahaan : </b> ".$row->nama_icon
												
												];
					$data['geometry']=[
												"type" => "Point",
												"coordinates" => [$row->longitude,$row->latitude ] 
												];	

					$response[]=$data;
				}
				echo json_encode($response,JSON_PRETTY_PRINT);	
			}
//menampilkan data perusahaan sesuai kategori jenis perusahaan dari id_icon dan digabungkan dengan m_perusahaan
			if($type=='varpoint'){
				if($id!=''){
					$this->db->where('a.id_icon',$id);
				}
				$getPerusahaan=$this->m_perusahaan->get();
				foreach ($getPerusahaan->result() as $row) {
					$data=null;
					$data['type']="Feature";
					$data['properties']=[
												"name"=>$row->nama_tempat,
												"lokasi"=>$row->alamat.' Kec. '.$row->n_kabupaten,
												"link"=>$row->id_perusahaan,
												"a"=>[$row->latitude,$row->longitude ],
												"gambar"=>($row->gambar=='')?('gambar/'):('gambar/'.$row->gambar),
												"icon"=>($row->icon=='')?base_url('marker'):base_url('marker/'.$row->icon),
												"popUp"=>"<b>Nama : </b>".$row->nama_tempat." </br><b>Kabupaten :</b> ".$row->n_kabupaten."<br><b>Jenis Perusahaan :</b> ".$row->nama_icon
												];
					$data['geometry']=[
												"type" => "Point",
												"coordinates" => [$row->longitude,$row->latitude ] 
												];	

					$response[]=$data;
				}

//menampilkan seluruh data perusahaan
				echo 'perusahaanPoint ='.json_encode($response,JSON_PRETTY_PRINT);	
			}
			elseif($type=="polygon"){
				$getPerusahaan=$this->m_perusahaan->get();
				$polygon=null;
				foreach ($getPerusahaan->result() as $row) {
					if($row->polygon!=NULL){
						$polygon[]=$row->polygon;
					}
				}
				echo "var latlngs=[".implode(',', $polygon)."];";
			}
			
		}
		
	}
}
