<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_perusahaan');
		$this->load->model('m_icon');
		$this->load->model('m_kabupaten');
		$this->load->model('m_berita');
		$this->load->model('m_gallery');

	}

	public function index()
	{
		$data = array(
			'title' => 'Selamat Datang di WebGIS Pariperusahaan Blora',
			'perusahaan' => $this->m_perusahaan->get_all_data(),		
			'berita'	=>$this->m_berita->latest_berita(),
			'isi'	=> 'v_home'
		);
		$this->load->view('layout/v_wrapper', $data, FALSE);
	}

	public function mapjson()
	{
		$data = array(
			'title' => 'Map JSON',
			'json' => $this->m_json->get_json(),
			'isi'	=> 'v_mapjson'
		);
		$this->load->view('layout/v_wrapper', $data, FALSE);
	}

	public function perusahaan()
	{
		$data = array(
			'title' => 'List Tempat perusahaan',
			'perusahaan' => $this->m_perusahaan->get_all_data(),
			'isi'	=> 'v_perusahaan'
		);
		$this->load->view('layout/v_wrapper', $data, FALSE);
	}
	public function perusahaankabupaten($data)
	{
		$data = array(
			'title' => 'List perusahaan di kabupaten',
			'perusahaankabupaten' => $this->m_kabupaten->perusahaankabupaten($data),
			'isi'	=> 'v_perusahaan_kabupaten'
		);
		$this->load->view('layout/v_wrapper', $data, FALSE);
	}
	public function pemetaan()
	{
		$data = array(
			'title' => 'Pemetaan Tempat perusahaan',
			'hitung'	=> $this->m_kabupaten->hitung(),
			'hitung1'	=> $this->m_icon->hitung(),
			'perusahaan'	=> $this->m_perusahaan->get_all_data(),
			'isi'	=> 'v_pemetaan'
		);
		$this->load->view('layout/v_wrapper', $data, FALSE);
	}
	public function detail($id_perusahaan)
	{
		$perusahaan = $this->m_perusahaan->detail($id_perusahaan);
		$foto = $this->m_gallery->foto($id_perusahaan);
		$data = array(
			'perusahaan' => $perusahaan,
			'foto' => $foto,
			'title' =>  $perusahaan->nama_tempat,
			'isi'	=> 'v_detail'
		);
		$this->load->view('layout/v_wrapper', $data, FALSE);
	}

	public function berita()
	{
		$this->load->library('pagination');
		$config['base_url'] = base_url('home/berita');
		$config['total_rows'] = count($this->m_berita->total_berita());
		$config['per_page'] = 9;
		$config['uri_segmen']= 3;
		//start dan limit
			$limit= $config['per_page'];
			$start= ($this->uri->segment(3)) ? ($this->uri->segment(3)) : 0;
		//--------------------
		$config['first_link']		= 'First';
		$config['last_link']		= 'Last';
		$config['next_link']		= 'Next';
		$config['prev_link']		= 'Prev';
		$config['full_tag_open']	= '<div class="pagination_container d-flex flex-row align-items-center justify-content-start text-center"><ul class="pagination_list">';
		$config['num_tag_open']		= '<li>';
		$config['num_tag_close']	= '</li>';
		$config['cur_tag_open']		= '<li class="active"><a>';
		$config['cur_tag_close']	= '</a></li>';
		$config['next_tag_open']	= '<li>';
		$config['next_tag_close']	= '</li>';
		$config['prev_tag_open']	= '<li>';
		$config['prev_tag_close']	= '</li>';
		$config['firts_tag_open']	= '<li>';
		$config['firts_tag_close']	= '</li>';
		$config['last_tag_open']	= '<li>';
		$config['last_tag_close']	= '</li>';
		$config['full_tag_close']	= '</ul></div>';
		//------------------------
		$this->pagination->initialize($config);

		$data = array(
			'paginasi'		=> $this->pagination->create_links(),
			'latest_berita'	=>$this->m_berita->latest_berita(),
			'beritaku'	=>$this->m_berita->beritaku(),
			'berita' 		=> $this->m_berita->berita($limit,$start),
			'title' 		=> 'Berita',
			'isi'			=> 'v_berita'
		);
		$this->load->view('layout/v_wrapper',$data,FALSE);
	}

	public function detail_berita($slug_berita)
	{
		$data = array(
			'title' 	=> 'Detail Berita',
			'latest_berita'	=>$this->m_berita->latest_berita(),
			'beritaku'	=>$this->m_berita->beritaku(),
			'berita' 	=> $this->m_berita->detail_berita($slug_berita),
			'isi'		=> 'v_detail_berita'
		);
		$this->load->view('layout/v_wrapper',$data,FALSE);
	}



}
