<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $title1?>
        <small>it all starts here</small>
      </h1>
    </section>



    <!-- Main content -->
    <section class="content">

    <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php echo $total?></h3>

              <p>Total Perusahaan</p>
            </div>
            <div class="icon">
              <i class="ion ion-map"></i>
            </div>
            <a href="<?= base_url() ?>perusahaan" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div> 
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo $totalkabupaten ?><sup style="font-size: 20px"></sup></h3>

              <p>Total Kabupaten</p>
            </div>
            <div class="icon">
              <i class="ion ion-paper-airplane"></i>
            </div>
            <a href="<?= base_url() ?>kabupaten" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?php echo $totalkategori?></h3>

              <p>Kategori Perusahaan</p>
            </div>
            <div class="icon">
              <i class="ion ion-bookmark"></i>
            </div>
            <a href="<?= base_url() ?>icon" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
    
        <!-- ./col -->
      </div>


      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
