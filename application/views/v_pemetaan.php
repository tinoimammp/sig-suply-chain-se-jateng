    <!-- ======= Breadcrumbs ======= -->
    <style type="text/css">
    
    .search-tip b {
      display: inline-block;
      clear: left;
      float: right;
      padding: 0 4px;
      margin-left: 4px;
    }

    .Banjir.search-tip b,
    .Banjir.leaflet-marker-icon {
      background: #f66
    }
  </style>
    <section class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2><?php echo $title ?></h2>
          <ol>
            <li><a href="<?= base_url() ?>">Home</a></li>
            <li><?php echo $title ?></li>
          </ol>
        </div>

      </div>
    </section>
    <!-- End Breadcrumbs -->

    <!-- ======= Services Section ======= -->
    <section id="services" class="services">
      <div class="container">

        <div class="section-title">
          <h2>Jenis Perusahaan</h2>
        </div>

        <div class="row">
          
<?php $no = 1;
      foreach ($hitung1 as $key => $value) { ?>
          <div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
            <div class="icon-box iconbox-blue">
              <div class="icon">
            
              <img src="<?= base_url('marker/' . $value->icon)  ?>" width="40px">
               </div>
              <h4><a href=""><h5> <?= $value->nama_icon ?></a></h4>
              <p><?= $value->keterangan ?></p>
            </div>
          </div>         

          <?php } ?><!-- End Stats Item -->

        </div>

      </div>
    </section><!-- End Services Section -->
    <section class="inner-page">
      <div class="container">
        
        <div class="map" data-aos="fade-up">

        <div id="map"></div>


      </div>

    </section>
<script src="<?=base_url('api/data/kabupaten')?>"></script>
  <script src="<?=base_url('api/data/perusahaan/varpoint')?>"></script>
  <script src="<?=base_url('api/data/icon')?>"></script>
 <script type="text/javascript">
    var map = L.map('map', {
    fullscreenControl: {
        pseudoFullscreen: true
    }
  }).setView([-7.20851866,110.2415984], 9);
    var layersIcon=[];
    var Layer=L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
    map.options.minZoom = 9;
  map.addLayer(Layer);

  // pengaturan legend

  function iconByName(name) {
    return '<i class="icon" style="background-color:'+name+';border-radius:50%"></i>';
  }


  function iconByImage(image) {
    return '<img src="'+image+'" style="width:16px">';
  }


  var baseLayers = [

    {
      name: "OpenStreetMap",
      layer: Layer
    },
    { 
      name: "Satellite",
      layer: L.tileLayer('https://www.google.cn/maps/vt?lyrs=s@189&gl=cn&x={x}&y={y}&z={z}')
    }
  ];
  // Akhir konfigurasi
  
    //kategori hotspot
    
  for(i=0;i<dataIcon.length;i++){
    var data=dataIcon[i];
    var layer={
      name: data.nama_icon,
      marker: iconByImage(data.icon),
      layer: new L.GeoJSON.AJAX(["<?=site_url('api/data/perusahaan/point')?>/"+data.id_icon],
        {
           pointToLayer: function (feature, latlng) {
              // console.log(feature)
                return L.marker(latlng, {
                  icon : new L.icon({
                      iconUrl: feature.properties.icon,
                    iconSize: [33,44]
                      })
                });
            },
            onEachFeature: function(feature,layer){
               if (feature.properties && feature.properties.name) {
                  layer.bindPopup('<img src="<?= base_url() ?>'+ feature.properties.gambar +'" style="width:100%;height:100px;"> </br></br>' + 

                    feature.properties.popUp +
                   "<center><br> <a  href='detail/" + feature.properties.link + "' class='btn btn-sm btn-outline-success btn-block' target='_blank'>DETAIL</a>"
                + " <a  href='https://www.google.com/maps/dir/?api=1&origin=" + "&destination=" + feature.properties.a  + "' class='btn btn-sm btn-outline-primary btn-block' target='_blank'>RUTE</a></center>"

                    );
              }
            }
        }).addTo(map)
      }
    layersIcon.push(layer);
  }

  // hostpot
  var layersPerusahaanPoint=L.geoJSON(perusahaanPoint, {
      pointToLayer: function (feature, latlng) {
        // console.log(feature)
          return L.marker(latlng, {
            icon : new L.icon({
                iconUrl: feature.properties.icon,
              iconSize: [33, 44]
                })
          });
      },
      onEachFeature: function(feature,layer){
         if (feature.properties && feature.properties.name) {
            layer.bindPopup('<img src="<?= base_url() ?>'+ feature.properties.gambar +'" style="width:100%;height:100px;"> </br></br>' + 

                    feature.properties.popUp +
                   "<center><br> <a  href='detail/" + feature.properties.link + "' class='btn btn-sm btn-outline-success btn-block' target='_blank'>DETAIL</a>"
                + " <a  href='https://www.google.com/maps/dir/?api=1&origin=" + "&destination=" + feature.properties.a  + "' class='btn btn-sm btn-outline-primary btn-block' target='_blank'>RUTE</a></center>"

                    );
        }
      }
  }).addTo(map);
  // akhir dari hotspot
  // pencarian
  var poiLayers = L.layerGroup([
    layersPerusahaanPoint
  ]);
  L.control.search({
    layer: poiLayers,
    initial: false,
    propertyName: 'name',
    zoom:14,
    buildTip: function(text, val) {
      // var jenis = val.layer.feature.properties.jenis;
      // return '<a href="#" class="'+jenis+'">'+text+'<b>'+jenis+'</b></a>';
      return '<a href="#" >'+text+'</a>';
    },
    marker: {
      icon: "",
      circle: {
        radius: 30,
        fillColor: 'green',
        weight:5
      }
    }
  }).addTo(map);
  // end pencarian

<?php foreach ($hitung as $key => $value) { ?>

   $.getJSON("<?= base_url('geojson/' . $value->geojson_kabupaten) ?>", function(data) {
    geoLayer = L.geoJson(data, {

      style: function(feature) {
        return {
           opacity: 0.1,
        color: 'green',
        fillOpacity: 0.2,
        fillColor: '<?= $value->kode_warna ?>',
       
        }
      },
    }).addTo(map);

    // geoLayer.eachLayer(function(layer){
      

    //   layer.bindPopup("Kabupaten : <?= $value->n_kabupaten ?> <br>" +  
    //                   "Total Perusahaan : <?= $value->totalkabupaten ?> <br> <br>" +
    //                   "<center><a  href='<?= base_url('home/perusahaankabupaten/' . $value->id_kabupaten) ?>' class='btn btn-sm btn-outline-primary'>Detail</a></center>"

    //     );

    //    layer.bindTooltip("<?= $value->n_kabupaten ?> <br>", {
    //    permanent:true,
    //         direction:"center",
    //         className:"no-background"
    //            });

    // });
   });
 <?php }?>

  // registrasikan untuk panel layer
  var overLayers = [{
    group: "Kategori Perusahaan",
    layers: layersIcon
  },{
    group: "Data Perusahaan",
    layers: [{
          name: "Semua Perusahaan",
          layer: layersPerusahaanPoint
      }]
  }
  ];

  var panelLayers = new L.Control.PanelLayers(baseLayers, overLayers,{
    collapsibleGroups: true

  });

  map.addControl(panelLayers);
  // end registrasikan untuk panel layer



   </script>  </main><!-- End #main -->
