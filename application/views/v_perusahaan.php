  <main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <div class="breadcrumbs d-flex align-items-center" style="background-image: url('<?= base_url() ?>gambar/foto4.png');">
      <div class="container position-relative d-flex flex-column align-items-center" data-aos="fade">

        <h2>Daftar Perusahaan</h2>
        <ol>
          <li><a href="<?= base_url()?>">Home</a></li>
          <li>Perusahaan</li>
        </ol>

      </div>
    </div><!-- End Breadcrumbs -->
    <!-- ======= Our Projects Section ======= -->
    <section id="projects" class="projects">
      <div class="container" data-aos="fade-up">

        <div class="portfolio-isotope" data-portfolio-filter="*" data-portfolio-layout="masonry" data-portfolio-sort="original-order">

          <div class="row gy-4 portfolio-container" data-aos="fade-up" data-aos-delay="200">
<?php $no = 1;
					foreach ($perusahaan as $key => $value) { ?>
            <div class="col-lg-4 col-md-6 portfolio-item filter-remodeling">
              <div class="portfolio-content h-100">
                <img style="width: 400px; height: 400px;" src="<?= base_url('gambar/' . $value->gambar)  ?>" class="img-fluid" alt="">
                <div class="portfolio-info">
                  <h4><?= $value->nama_icon ?></h4>
                  <p><?= $value->nama_tempat ?></p>
                  <a href="<?= base_url('gambar/' . $value->gambar)  ?>" title="<?= $value->alamat ?>" data-gallery="<?= $value->alamat ?>" class="glightbox preview-link"><i class="bi bi-zoom-in"></i></a>
                  <a href="<?= base_url('home/detail/' . $value->id_perusahaan) ?>" title="More Details" class="details-link"><i class="bi bi-link"></i></a>

                </div>
              </div>
            </div><!-- End Projects Item -->
            					<?php } ?>

             </div><!-- End Projects Container -->

        </div>

      </div>

    </section><!-- End Our Projects Section -->

  </main><!-- End #main -->
