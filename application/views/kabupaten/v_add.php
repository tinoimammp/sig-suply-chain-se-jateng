<div class="content-wrapper">
 <!-- Main content -->
  <section class="content-header">
      <h1>
        <?php echo $title?>
        <small>it all starts here</small>
      </h1>
    </section>
    <section class="content">
    	<div class="row">
<div class="col-md-6">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
<div class="form-horizontal">
	             <div class="box-body">
			<?php
			echo validation_errors('<div class="alert alert-warning alert-dismissible">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>', '</div>');
			if (isset($error_upload)) {
				echo '<div class="alert alert-danger alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' . $error_upload . '</div>';
			}


			echo form_open_multipart('kabupaten/add');
			?>

			<div class="form-group">
                  
                  <label  class="col-sm-2 control-label">Nama Kabupaten</label>

                  <div class="col-sm-10">
                    <input name="n_kabupaten" type="text" class="form-control" placeholder="Nama Kabupaten" required>
                  </div>
                </div>
                 <div class="form-group">
                  
                  <label  class="col-sm-2 control-label">Kode Kabupaten</label>

                  <div class="col-sm-10">
                    <input name="kd_kabupaten" type="text" class="form-control" placeholder="Kode Kabupaten" required>
                  </div>
                </div>
                <!-- <div class="form-group">
                  
                  <label  class="col-sm-2 control-label">Kode Warna</label>

                  <div class="col-sm-10">
                    <input name="kode_warna" type="text" class="form-control" placeholder="#0000000" required>
                  </div>
                </div>
                <div class="form-group">
                  
                  <label  class="col-sm-2 control-label">File GeoJSON</label>

                  <div class="col-sm-10">
        <input type="file" name="geojson_kabupaten" placeholder="" class="form-control" required>
                  </div>
                </div> -->

        <div class="box-footer">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-info pull-right">Submit</button>
              </div>   
              <?php echo form_close(); ?>

              </div>
            </div>
              <!-- /.box-body -->
              
              <!-- /.box-footer -->

          </div>
      </div>          
  </section>
          <!-- /.box -->


</div>