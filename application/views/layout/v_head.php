<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

	<title><?= $title  ?></title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="<?= base_url() ?>frontend/assets/img/favicon.png" rel="icon">
  <link href="<?= base_url() ?>frontend/assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,600;1,700&family=Roboto:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&family=Work+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <!-- <link href="<?= base_url() ?>frontend/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?= base_url() ?>frontend/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="<?= base_url() ?>frontend/assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
  <link href="<?= base_url() ?>frontend/assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="<?= base_url() ?>frontend/assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="<?= base_url() ?>frontend/assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet"> -->
  <link rel="stylesheet" href="<?= base_url() ?>geojson/leaflet-search.css" />
  <link rel="stylesheet" href="https://leaflet.github.io/Leaflet.fullscreen/dist/leaflet.fullscreen.css" />
    <link rel="stylesheet" href="<?= base_url() ?>geojson/leaflet-panel-layers-master/src/leaflet-panel-layers.css" />
<!-- Assets Front End -->

 <!-- Favicons -->
 <link href="<?= base_url() ?>frontend/assets/img/favicon.png" rel="icon">
  <link href="<?= base_url() ?>frontend/assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="<?= base_url() ?>frontend/assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="<?= base_url() ?>frontend/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?= base_url() ?>frontend/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="<?= base_url() ?>frontend/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="<?= base_url() ?>frontend/assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="<?= base_url() ?>frontend/assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="<?= base_url() ?>frontend/assets/css/style1.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Green - v4.8.0
  * Template URL: https://bootstrapmade.com/green-free-one-page-bootstrap-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
<!-- Eend Assets Front End -->
<!-- Leaflet-->
<script
			  src="https://code.jquery.com/jquery-3.6.0.js"
			  integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
			  crossorigin="anonymous"></script>  <!-- Template Main CSS File -->
  <link href="<?= base_url() ?>frontend/assets/css/main.css" rel="stylesheet">
      <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css" />
  <link rel="stylesheet" href="<?=base_url()?>geojson/leaflet-panel-layers-master/src/leaflet-panel-layers.css" />
  <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"></script>
    <link rel="stylesheet" href="<?= base_url() ?>geojson/leaflet-search.css" />
 <script src="<?= base_url() ?>geojson/leaflet.ajax.js"></script>
  <script src="<?= base_url() ?>geojson/leaflet.ajax.min.js"></script>
  <script src="<?= base_url() ?>geojson/leaflet-search.js"></script>
  <script src="https://leaflet.github.io/Leaflet.fullscreen/dist/Leaflet.fullscreen.min.js"></script>
  <script src="<?=base_url('geojson/node_modules/leaflet-easyprint/dist/bundle.js')?>"></script>
  <link rel="stylesheet" href="<?=base_url('geojson/leaflet-compass-master/src/leaflet-compass.css')?>" />
  <script src="<?=base_url('geojson/leaflet-panel-layers-master/src/leaflet-panel-layers.js')?>"></script>
  <script src="<?=base_url('geojson/Leaflet.GoogleMutant.js')?>"></script>
  <script src="<?=base_url('geojson/node_modules/leaflet-easyprint/dist/bundle.js')?>"></script>
  <script src="<?=base_url('geojson/leaflet-search/dist/leaflet-search.src.js')?>"></script>
    <link rel="stylesheet" type="text/css" href="<?=base_url('geojson/leaflet-search/dist/leaflet-search.min.css')?>">
  <!-- =======================================================
  * Template Name: UpConstruction - v1.0.1
  * Template URL: https://bootstrapmade.com/upconstruction-bootstrap-construction-website-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
     <style type="text/css">
        #map { height: 100vh; 
border-style: solid;
        }
        #map { 
border-style: solid;
        }
        .iconsearch {
      display: inline-block;
      margin: 2px;
      height: 16px;
      width: 16px;
      background-color: #ccc;
      }
      .icon-bar {
        background: url('geojson/leaflet-panel-layers-master/examples/images/icons/bar.png') center center no-repeat;
    }
    .leaflet-tooltip.no-background{
      background: transparent;
      border:0;
      box-shadow: none;
      color: #fff;
      font-weight: bold;
      text-shadow: 1px 1px 1px #000,-1px 1px 1px #000,1px -1px 1px #000,-1px -1px 1px #000;
    }
    </style>
</head>