    <!-- ======= Breadcrumbs ======= -->
    <div class="breadcrumbs d-flex align-items-center" style="background-image: url('<?= base_url() ?>gambar/foto4.png');">
      <div class="container position-relative d-flex flex-column align-items-center" data-aos="fade">

        <h2>Berita</h2>
        <ol>
          <li><a href="<?=base_url() ?>">Home</a></li>
          <li>Berita</li>
        </ol>

      </div>
    </div><!-- End Breadcrumbs -->




<!-- ======= Recent Blog Posts Section ======= -->
    <section id="recent-blog-posts" class="blog">
      <div class="container" data-aos="fade-up" >   
    
      <div class="row gy-5 posts-list">
                              <?php foreach ($berita as $key => $value) { ?>

        <div class="col-xl-4 col-md-6" data-aos="fade-up" data-aos-delay="100">

            <div class="post-item position-relative h-100">

            <div class="post-img position-relative overflow-hidden">
              <img src="<?= base_url('gambar_berita/'.$value->gambar_berita) ?>" class="img-fluid" alt="">
              <span class="post-date"><?= $value->tgl_berita ?></span>
            </div>
            <div class="post-content d-flex flex-column">

              <h3 class="post-title"><?= $value->judul_berita ?></h3>

              <div class="meta d-flex align-items-center">
                <div class="d-flex align-items-center">
                  <i class="bi bi-person"></i> <span class="ps-2">Administrator</span>
                </div>
                <span class="px-3 text-black-50"></span>
                <div class="d-flex align-items-center">
                </div>
              </div>

              <hr>
                      <p><?= substr(strip_tags($value->isi_berita),0,100) ?>..</p>
                                  </br>

              <a href="<?= base_url('home/detail_berita/'.$value->slug_berita) ?>" class="readmore stretched-link"><span>Read More</span><i class="bi bi-arrow-right"></i></a>

            </div>
                          


        

      </div>

      </div><?php } ?>
      </div>
    <!-- End Recent Blog Posts Section -->
     <div class="blog-pagination">
          <ul class="justify-content-center">
           <?php 
                    if (isset($paginasi)) {
                      echo $paginasi;
                    }

                  ?>
          </ul>
        </div><!-- End blog pagination -->



      </div>
    </section><!-- End Blog Section -->


