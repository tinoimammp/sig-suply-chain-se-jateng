  <main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <div class="breadcrumbs d-flex align-items-center" style="background-image: url('<?= base_url() ?>gambar/foto4.png');">
      <div class="container position-relative d-flex flex-column align-items-center" data-aos="fade">

        <h2>Detail Berita</h2>
        <ol>
          <li><a href="<?= base_url()?>">Home</a></li>
          <li>Detail</li>
        </ol>

      </div>
    </div><!-- End Breadcrumbs -->



 <!-- ======= Blog Details Section ======= -->
    <section id="blog" class="blog">
      <div class="container" data-aos="fade-up" data-aos-delay="100">

        <div class="row g-5">

          <div class="col-lg-8">

            <article class="blog-details">

              <div class="post-img">
                <img src="<?= base_url('gambar_berita/'.$berita->gambar_berita) ?>" alt="" class="img-fluid">
              </div>

              <h2 class="title"><?= $berita->judul_berita ?></h2>

              <div class="meta-top">
                <ul>
                  <li class="d-flex align-items-center"><i class="bi bi-person"></i> <a href="blog-details.html">Administrator</a></li>
                  <li class="d-flex align-items-center"><i class="bi bi-clock"></i> <a href="blog-details.html"><time datetime="<?= $berita->tgl_berita ?>"><?= $berita->tgl_berita ?></time></a></li>
                 
                </ul>
              </div><!-- End meta top -->

              <div class="content">
                <p><?= $berita->isi_berita ?></p>
              </div><!-- End post content -->
        <!-- End meta bottom -->
        

            </article><!-- End blog post -->
          
        </div>
 <div class="col-lg-4">

            <div class="sidebar">

              

              <!-- End sidebar categories-->

              <div class="sidebar-item recent-posts">
                <h3 class="sidebar-title">Berita Terbaru</h3>
									<?php foreach ($beritaku as $key => $value) { ?>
                <div class="mt-3">

                  <div class="post-item mt-3">
                    <img src="<?= base_url('gambar_berita/'.$value->gambar_berita) ?>" alt="">
                    <div>
                      <h4><a href="<?= base_url('home/detail_berita/'.$value->slug_berita) ?>"><?= $value->judul_berita ?></a></h4>
                      <time datetime="<?= $value->tgl_berita ?>"><?= $value->tgl_berita ?></time>
                    </div>
                  </div><!-- End recent post item-->

									<?php } ?>

                </div>

              </div><!-- End sidebar recent posts-->
            </div><!-- End Blog Sidebar -->

          </div>
      </div>
                </section><!-- End Blog Details Section -->
                  </main><!-- End #main -->

