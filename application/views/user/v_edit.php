<div class="content-wrapper">
 <!-- Main content -->
  <section class="content-header">
      <h1>
        <?php echo $title?>
        <small>it all starts here</small>
      </h1>
    </section>
    <section class="content">
    	<div class="row">
<div class="col-md-6">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
<div class="form-horizontal">
	             <div class="box-body">
			<?php
			echo validation_errors('<div class="alert alert-warning alert-dismissible">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>', '</div>');
			if (isset($error_upload)) {
				echo '<div class="alert alert-danger alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' . $error_upload . '</div>';
			}


			echo form_open_multipart('user/edit/' . $user->id_user);
			?>
			<div class="form-group">
                	
                  <label  class="col-sm-2 control-label">Nama</label>

                  <div class="col-sm-10">
						<input name="nama_user" value="<?= $user->nama_user ?>" placeholder="Nama User" type="text" class="form-control">
                  </div>
                </div>
                	<div class="form-group">
                	
                  <label  class="col-sm-2 control-label">Email</label>

                  <div class="col-sm-10">
						<input name="email" value="<?= $user->email ?>" placeholder="Nama User" class="form-control">
                  </div>
                </div>
                	<div class="form-group">
                	
                  <label  class="col-sm-2 control-label">Password</label>

                  <div class="col-sm-10">
						<input name="password" value="<?= $user->password ?>" type="password" placeholder="Password" class="form-control">
                  </div>
                </div>
                	<div class="form-group">
                	
                  <label  class="col-sm-2 control-label">No. Telp</label>

                  <div class="col-sm-10">
						<input name="no_telpon" value="<?= $user->no_telpon ?>" placeholder="No Telpon" class="form-control">
                  </div>
                </div>

                <div class="form-group">
                	
                  <label  class="col-sm-2 control-label">Foto</label>

                  <div class="col-sm-10">
				<img src="<?= base_url('foto/' . $user->foto) ?>" width="100px">
                  </div>
                </div>
                <div class="form-group">
                	
                  <label  class="col-sm-2 control-label">Ganti Foto</label>

                  <div class="col-sm-10">
				<input type="file" name="foto" placeholder="No Telpon" class="form-control">
                  </div>
                </div>

        <div class="box-footer">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-info pull-right">Submit</button>
              </div>   
              <?php echo form_close(); ?>

              </div>
            </div>
              <!-- /.box-body -->
              
              <!-- /.box-footer -->

          </div>
      </div>          
  </section>
          <!-- /.box -->


</div>
