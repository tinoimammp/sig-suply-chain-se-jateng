
  <!-- ======= Hero Section ======= -->
  <section id="hero">
    <div id="heroCarousel" data-bs-interval="5000" class="carousel slide carousel-fade" data-bs-ride="carousel">

      <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>

      <div class="carousel-inner" role="listbox">

        <!-- Slide 1 -->
        <div class="carousel-item active" style="background-image: url(frontend/assets/img/slide/slide3.jpeg)">
          <div class="carousel-container">
            <div class="container">
              <h2 class="animate__animated animate__fadeInDown">Sistem Informasi Pemetaan Perusahaan<br><span> Konstruksi Se-Jawa Tengah</span></h2>
              <p class="animate__animated animate__fadeInUp">Perusahaan adalah tempat terjadinya kegiatan produksi dan berkumpulnya semua faktor produksi barang dan jasa. 
                <br>Ada perusahaan yang terdaftar di pemerintah dan ada pula yang tidak. </p>
              <a href="<?= base_url('home/pemetaan') ?>" class="btn-get-started animate__animated animate__fadeInUp scrollto">Lihat Pemetaan</a>
            </div>
          </div>
        </div>

        <!-- Slide 2 -->
        <div class="carousel-item" style="background-image: url(frontend/assets/img/slide/slide4.jpeg)">
          <div class="carousel-container">
          <div class="container">
              <h2 class="animate__animated animate__fadeInDown">Sistem Informasi Pemetaan Perusahaan<br><span> Konstruksi Se-Jawa Tengah</span></h2>
              <p class="animate__animated animate__fadeInUp">Perusahaan adalah tempat terjadinya kegiatan produksi dan berkumpulnya semua faktor produksi barang dan jasa. 
                <br>Ada perusahaan yang terdaftar di pemerintah dan ada pula yang tidak. </p>
              <a href="<?= base_url('home/pemetaan') ?>" class="btn-get-started animate__animated animate__fadeInUp scrollto">Lihat Pemetaan</a>
            </div>
          </div>
        </div>

        <!-- Slide 3 -->
        <div class="carousel-item" style="background-image: url(frontend/assets/img/slide/slide2.jpg)">
          <div class="carousel-container">
          <div class="container">
              <h2 class="animate__animated animate__fadeInDown">Sistem Informasi Pemetaan Perusahaan<br><span> Konstruksi Se-Jawa Tengah</span></h2>
              <p class="animate__animated animate__fadeInUp">Perusahaan adalah tempat terjadinya kegiatan produksi dan berkumpulnya semua faktor produksi barang dan jasa. 
                <br>Ada perusahaan yang terdaftar di pemerintah dan ada pula yang tidak. </p>
              <a href="<?= base_url('home/pemetaan') ?>" class="btn-get-started animate__animated animate__fadeInUp scrollto">Lihat Pemetaan</a>
            </div>
          </div>
        </div>

      </div>

      <a class="carousel-control-prev" href="#heroCarousel" role="button" data-bs-slide="prev">
        <span class="carousel-control-prev-icon bi bi-chevron-left" aria-hidden="true"></span>
      </a>

      <a class="carousel-control-next" href="#heroCarousel" role="button" data-bs-slide="next">
        <span class="carousel-control-next-icon bi bi-chevron-right" aria-hidden="true"></span>
      </a>

    </div>
  </section><!-- End Hero -->

  <main id="main">
    <br>
       <!-- ======= Icon Boxes Section ======= -->
    <section id="icon-boxes" class="icon-boxes">
      <div class="container">

        <div class="row">
          <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0" data-aos="fade-up">
            <div class="icon-box">
              <div class="icon"><i class="bx bxl-dribbble"></i></div>
              <h4 class="title"><a href="">Perusahaan</a></h4>
              <p class="description">Perusahaan adalah tempat terjadinya kegiatan produksi dan berkumpulnya semua faktor produksi barang dan jasa. </p>
          </div>
          </div>

          <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0" data-aos="fade-up" data-aos-delay="100">
            <div class="icon-box">
              <div class="icon"><i class="bx bx-file"></i></div>
              <h4 class="title"><a href="">Batching Plant</a></h4>
              <p class="description">Tempat untuk produksi beton ready mix dalam jumlah yang besar dari jenis beton yang dihasilkan atau dari jenis pengoperasiannya</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0" data-aos="fade-up" data-aos-delay="200">
            <div class="icon-box">
              <div class="icon"><i class="bx bx-tachometer"></i></div>
              <h4 class="title"><a href="">Asphalt Mixing Plant</a></h4>
              <p class="description">Seperangkat peralatan yang mempunyai fungsi untuk memproduksi bahan pelapisan permukaan jalan lentur yaitu campuran beraspal panas.</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0" data-aos="fade-up" data-aos-delay="300">
            <div class="icon-box">
              <div class="icon"><i class="bx bx-layer"></i></div>
              <h4 class="title"><a href="">Quarry Material</a></h4>
              <p class="description">Quarry adalah lokasi pertambangan tanah atau batuan yang digunakan untuk keperluan proyek seperti tanah material timbunan, dan batu.</p>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Icon Boxes Section -->



    <!-- ======= Our Clients Section ======= -->
    <section id="clients" class="clients">
      <div class="container">

        <div class="section-title">
          <h2>Perusahaan Terdaftar</h2>
          <p></p>
        </div>

        <div class="clients-slider swiper">
          <div class="swiper-wrapper align-items-center">
            <div class="swiper-slide"><img src="frontend/assets/img/clients/bumn.png" class="img-fluid" alt=""></div>
            <div class="swiper-slide"><img src="frontend/assets/img/clients/wika.png" class="img-fluid" alt=""></div>
            <div class="swiper-slide"><img src="frontend/assets/img/clients/client-3.png" class="img-fluid" alt=""></div>
            <div class="swiper-slide"><img src="frontend/assets/img/clients/client-4.png" class="img-fluid" alt=""></div>
            <div class="swiper-slide"><img src="frontend/assets/img/clients/client-5.png" class="img-fluid" alt=""></div>
            <div class="swiper-slide"><img src="frontend/assets/img/clients/client-6.png" class="img-fluid" alt=""></div>
            <div class="swiper-slide"><img src="frontend/assets/img/clients/client-7.png" class="img-fluid" alt=""></div>
            <div class="swiper-slide"><img src="frontend/assets/img/clients/client-8.png" class="img-fluid" alt=""></div>
          </div>
          <div class="swiper-pagination"></div>
        </div>

      </div>
    </section><!-- End Our Clients Section -->

    