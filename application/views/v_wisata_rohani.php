  <main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <div class="breadcrumbs d-flex align-items-center" style="background-image: url('<?= base_url() ?>frontend/assets/img/breadcrumbs-bg.jpg');">
      <div class="container position-relative d-flex flex-column align-items-center" data-aos="fade">

        <h2>Daftar Wisata</h2>
        <ol>
          <li><a href="<?= base_url()?>">Home</a></li>
          <li>Wisata</li>
        </ol>

      </div>
    </div><!-- End Breadcrumbs -->
    <!-- ======= Our Projects Section ======= -->
    <section id="projects" class="projects">
      <div class="container" data-aos="fade-up">

        <div class="portfolio-isotope" data-portfolio-filter="*" data-portfolio-layout="masonry" data-portfolio-sort="original-order">

          <div class="row gy-4 portfolio-container" data-aos="fade-up" data-aos-delay="200">
<?php $no = 1;
					foreach ($wisata as $key => $value) { ?>
            <div class="col-lg-4 col-md-6 portfolio-item filter-remodeling">
              <div class="portfolio-content h-100">
                <img style="width: 400px; height: 400px;" src="<?= base_url('gambar/' . $value->gambar)  ?>" class="img-fluid" alt="">
                <div class="portfolio-info">
                  <h4><?= $value->nama_icon ?></h4>
                  <p><?= $value->nama_tempat ?></p>
                  <a id="map" href="<?= base_url('gambar/' . $value->gambar)  ?>" title="<?= $value->alamat ?>" data-gallery="<?= $value->alamat ?>" class="glightbox preview-link"><i class="bi bi-zoom-in"></i></a>
                  <a href="<?= base_url('home/detail/' . $value->id_wisata) ?>" title="More Details" class="details-link"><i class="bi bi-link-45deg"></i></a>

                </div>
              </div>
            </div><!-- End Projects Item -->
            					<?php } ?>

             </div><!-- End Projects Container -->

        </div>

      </div>

    </section><!-- End Our Projects Section -->

  </main><!-- End #main -->



<script>
  navigator.geolocation.getCurrentPosition(function(location) {
    var latlng = new L.LatLng(location.coords.latitude, location.coords.longitude);
    var peta1 = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
      attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
        '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
        'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
      id: 'mapbox/streets-v11'
    });

    var peta2 = L.tileLayer('http://www.google.cn/maps/vt?lyrs=s@189&gl=cn&x={x}&y={y}&z={z}', {
      attribution: 'google'
    });

    var peta3 = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    });

    var map = L.map('map', {
      center: [<?= $wisata->latitude  ?>, <?= $wisata->longitude  ?>],
      zoom: 18,
      layers: [peta2]
    });

    var baseLayers = {
      "Grayscale": peta1,
      "Satelite": peta2,
      "Streets": peta3
    };
    L.control.layers(baseLayers).addTo(map);
    L.marker([<?= $wisata->latitude ?>, <?= $wisata->longitude ?>], {
      icon: L.icon({
        iconUrl: '<?= base_url('marker/' . $wisata->icon)  ?>',
        iconSize: [50, 50], // size of the icon
      })
    }).addTo(map).bindPopup("<img src='<?= base_url('gambar/' . $wisata->gambar) ?>' width='280px'>" +
      "Nama Tempat : <?= $wisata->nama_tempat ?></br>" +
      "Alamat : <?= $wisata->alamat ?></br>" +
      "Desa : <?= $wisata->desa ?></br>" +
      "<a href='https://www.google.com/maps/dir/?api=1&origin=" +
      location.coords.latitude + "," + location.coords.longitude + "&destination=<?= $wisata->latitude ?>,<?= $wisata->longitude ?>' class='btn btn-sm btn-outline-success' target='_blank'>Rute</a>").openPopup();
  });
</script>

