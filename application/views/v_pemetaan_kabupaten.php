  <main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <div class="breadcrumbs d-flex align-items-center" style="background-image: url('<?= base_url() ?>gambar/foto4.png');">
      <div class="container position-relative d-flex flex-column align-items-center" data-aos="fade">

        <h2><?php echo $title?></h2>
        <ol>
          <li><a href="<?= base_url()?>">Home</a></li>
          <li>Wisata</li>
        </ol>

      </div>
    </div><!-- End Breadcrumbs -->
    <!-- ======= Our Projects Section ======= -->
    <section id="projects" class="projects">
      <div class="container" data-aos="fade-up">

    <div id="map" style="width: 100%; height: 550px;"></div>


      </div>

    </section><!-- End Our Projects Section -->

  </main><!-- End #main -->




<script>
    var map = L.map('map').setView([-7.0928237, 111.3245501], 10);
   var tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
  }).addTo(map)

<?php foreach ($hitung as $key => $value) { ?>

   $.getJSON("<?= base_url('geojson/' . $value->geojson) ?>", function(data) {
    geoLayer = L.geoJson(data, {

      style: function(feature) {
        return {
           opacity: 0.1,
        color: 'green',
        fillOpacity: 0.5,
        fillColor: '<?= $value->kodewarna ?>',
       
        }
      },
    }).addTo(map);

    geoLayer.eachLayer(function(layer){
      

      layer.bindPopup("Kecamatan : <?= $value->n_kecamatan ?> <br>" +  
                      "Total Wisata : <?= $value->totalkec ?> <br> <br>" +
                      "<center><a  href='' class='btn btn-sm btn-outline-primary'>Detail</a></center>"

        );

       layer.bindTooltip("<?= $value->n_kecamatan ?> <br>", {
        permanent: true,
        direction: "center"
               });

    });
   });
 <?php }?>

</script>