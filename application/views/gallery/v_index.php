<div class="content-wrapper">
       <!-- Main content -->
    <section class="content">
<!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><?php echo $title?>
</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <?php

  if ($this->session->flashdata('pesan')) {
    echo '<div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
    echo $this->session->flashdata('pesan');
    echo '</div>';
  }

  ?>
        <div class="box-body table-responsive">
   <table id="example1" class="table table-hover">
                <thead>
                <tr>
                <th width="10px">No</th>
				<th>Nama Tempat</th>
				<th>Total Foto</th>
				<th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $no=1 ; foreach($gallery as $value){ ?>

                <tr>
                 <td><?php echo $no++; ?></td>
					<td><?php echo $value->nama_tempat; ?></td>
					<td><?php echo $value->total_foto; ?></td>
					<td><a href="<?php echo base_url('gallery/addfoto/'.$value->id_perusahaan); ?>" class="btn btn-success btn-sm"><i class="fa fa-photo"></i> Add Foto</a></td>
                </tr> 

            			<?php } ?>
			</tbody>
      </table>



         </div>
       
        <!-- /.box-footer-->

      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <div>
</div>
  <!-- /.content-wrapper -->
