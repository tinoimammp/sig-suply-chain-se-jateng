<div class="content-wrapper">
 <!-- Main content -->
  <section class="content-header">
      <h1>
Tambah Foto
        <small>it all starts here</small>
      </h1>
    </section>
    <section class="content">
    	<div class="row">

    		<div class="col-md-6">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
<div class="form-horizontal">
             <div class="box-body">
              <?php
			echo validation_errors('<div class="alert alert-warning alert-dismissible">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>', '</div>');
			if (isset($error_upload)) {
				echo '<div class="alert alert-danger alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' . $error_upload . '</div>';
			}


echo form_open_multipart('gallery/addfoto/'.$perusahaan->id_perusahaan);
			?>
              
                <div class="form-group">
                	
                  <label  class="col-sm-2 control-label">Keterangan</label>

                  <div class="col-sm-10">
<input name="ket_foto" type="text" class="form-control" placeholder="Keterangan Foto" value="<?php echo set_value('ket_foto'); ?>" required></div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Gambar</label>

                  <div class="col-sm-10">
          <input type="file" name="foto_perusahaan" class="form-control" required>
                  </div>
                </div>
                <div class="box-footer">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-info pull-right">Submit</button>
              </div>   
              <?php echo form_close(); ?>

              </div>
            </div>
              <!-- /.box-body -->
              
              <!-- /.box-footer -->

          </div>
      </div>          
  </section>
          <!-- /.box -->


<div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Gallery Foto</h3>
            </div>
                         <div class="box-body">

	<div class="col-sm-12">
		<?php foreach ($foto as $key => $value) { ?>
			<div class="col-sm-3">
			<h4><?php echo $value->ket_foto; ?><a href="<?php echo base_url('gallery/delete/'.$value->id_perusahaan.'/'.$value->id_foto); ?>" type="button" class="btn  btn-danger btn-xs" onclick="return confirm('Yakin Ingin Menghapus Data ini.?')" type="button" class="btn  btn-danger btn-sm" onclick="return confirm('Yakin Ingin Menghapus Foto ini.?')"><i class="fa fa-trash"></i></a></h4>

			<img width="200px" height="200px" src="<?php echo base_url('gambar/foto_perusahaan/'.$value->foto_perusahaan); ?>">
		</div>
		<?php } ?>
		
	</div>
</div>
</div>
</div>
</div>
</div>