<div class="content-wrapper">
 <!-- Main content -->
  <section class="content-header">
      <h1>
        <?php echo $title?>
        <small>it all starts here</small>
      </h1>
    </section>
    <section class="content">
    	<div class="row">
<div class="col-lg-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
<div class="form-horizontal">
             <div class="box-body">
              <?php
        echo validation_errors('<div class="alert alert-warning alert-dismissible">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>', '</div>');
        if (isset($error_upload)) {
          echo '<div class="alert alert-danger alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' . $error_upload . '</div>';
        }


			echo form_open_multipart('berita/edit/'.$berita->id_berita);
        ?>
              
                <div class="form-group">
                	
                  <label  class="col-sm-2 control-label"> Judul Berita</label>

                  <div class="col-sm-10">
				<input class="form-control" value="<?= $berita->judul_berita ?>" type="text" name="judul_berita" placeholder="Judul Berita" required> </div>
                </div>
                <div class="form-group">
                  <label  class="col-sm-2 control-label">Gambar</label>

                  <div class="col-sm-10">
					<input type="file" class="form-control" name="gambar_berita">
                  </div>
                </div>
                <div class="form-group">
                  <label  class="col-sm-2 control-label">Isi Berita</label>

                  <div class="col-sm-10">
 <textarea id="editor1" name="isi_berita" rows="10" cols="80"><?= $berita->isi_berita ?></textarea>   
              </div>
                </div>   
                

                
                
                  
                <div class="box-footer">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-info pull-right">Submit</button>
              </div>   
              <?php echo form_close(); ?>

              </div>
            </div>
              <!-- /.box-body -->
              
              <!-- /.box-footer -->

          </div>
      </div>          
  </section>
          <!-- /.box -->


</div>
