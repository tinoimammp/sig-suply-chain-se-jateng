<div class="content-wrapper">
       <!-- Main content -->
        <section class="content-header">
      <h1>
        <?php echo $title?>
        <small>it all starts here</small>
      </h1>
    </section>
    <section class="content">
<!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><?php echo $title?>
</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <?php

  if ($this->session->flashdata('pesan')) {
    echo '<div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
    echo $this->session->flashdata('pesan');
    echo '</div>';
  }

  ?>
        <div class="box-body table-responsive">
   <table id="example1" class="table table-hover">
                <thead>
                <tr>
                <th>No</th>
					<th>Judul Berita</th>
					<th>Tanggal</th>
					<th>Gambar</th>
					<th>Action</th>
                </tr>
                </thead>
                <tbody>
                			<?php $no=1; foreach ($berita as $key => $value) { ?>

                <tr>
                 <td><?= $no++; ?></td>
					<td><?= $value->judul_berita ?></td>
					<td><?= $value->tgl_berita ?></td>
					<td><img src="<?= base_url('gambar_berita/'.$value->gambar_berita) ?>" width="100px"></td>
					<td>
						<a href="<?= base_url('berita/edit/'.$value->id_berita) ?>" class="btn btn-xs btn-success"><i class="fa fa-pencil"></i> Ubah</a>
						<a href="<?= base_url('berita/delete/'.$value->id_berita) ?>" onclick="return confirm('Apakah Data Ini Akan Dihapus..?')" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> Hapus</a>
					</td>
                </tr> 

            			<?php } ?>
			</tbody>
      </table>



         </div>
       
        <!-- /.box-footer-->

      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
