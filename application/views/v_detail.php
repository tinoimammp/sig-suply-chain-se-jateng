

    <!-- ======= Breadcrumbs ======= -->
    <div class="breadcrumbs d-flex align-items-center" style="background-image: url('<?= base_url() ?>frontend/assets/img/slide/slide2.jpg');">
      <div class="container position-relative d-flex flex-column align-items-center" data-aos="fade">

        <h3>Detail Perusahaan : <?= $perusahaan->nama_tempat ?></h3>
        <ol>
          <li><a href="<?= base_url() ?>">Home</a></li>
          <li>Perusahaan</li>
        </ol>

      </div>
    </div><!-- End Breadcrumbs -->
 <section class="project-details">

    <!-- ======= Projet Details Section ======= -->

      <div class="container" data-aos="fade-up" data-aos-delay="100">

        <div class="position-relative h-100">
<div class="container" data-aos="fade-up" data-aos-delay="100">
        <div class="position-relative h-100">
          <div class="slides-2 portfolio-details-slider swiper">
            <div class="swiper-wrapper align-items-center portfolio-item">

        <?php foreach ($foto as $key => $value) { ?>

              <div class="swiper-slide">
                <img src="<?php echo base_url('gambar/foto_perusahaan/'.$value->foto_perusahaan); ?>" class="img-fluid" title="<?= $value->ket_foto ?>" alt="<?= $value->ket_foto ?>">                
              </div>
          <?php } ?>
            </div>
          </div>
          <div class="swiper-button-prev"></div>
          <div class="swiper-button-next"></div>

        </div>
        <div class="row justify-content-between gy-4 mt-4">
          <div class="col-lg-6">
            <div class="portfolio-info">
              <h2><?= $perusahaan->nama_tempat ?></h2>

        </div>
             <table class="table">
             <tr>
          <th>Alamat</th>
          <th>:</th>
          <td><?= $perusahaan->alamat ?></td>
        </tr>        
        <tr>
          <th>Jenis Perusahaan</th>
          <th>:</th>
          <td><?= $perusahaan->nama_icon ?></td>
        </tr>
        <tr>
          <th>Telepon</th>
          <th>:</th>
          <td><?= $perusahaan->telp ?></td>
        </tr>
        <tr>
          <th>E-Mail</th>
          <th>:</th>
          <td><?= $perusahaan->emailp ?></td>
        </tr>
        <tr>
          <th>Kabupaten</th>
          <th>:</th>
          <td><?= $perusahaan->n_kabupaten ?></td>
        </tr>
          <th>Provinsi</th>
          <th>:</th>
          <td><?= $perusahaan->prov ?></td>
        </tr>
      </table>
          </div>


          <div class="col-lg-6">
            <div class="portfolio-info">
              <h3>Maps Perusahaan</h3>
              <ul>
                     <div id="mymap" style="width: 100%; height: 400px;"></div>

              </ul>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Projet Details Section -->



  </main><!-- End #main -->



  <script>
  
    
var peta1 = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    });

    var peta2 = L.tileLayer('https://www.google.cn/maps/vt?lyrs=s@189&gl=cn&x={x}&y={y}&z={z}', {
      attribution: 'Satellite'
    });

    

    var mymap = L.map('mymap', {
      center: [<?= $perusahaan->latitude  ?>, <?= $perusahaan->longitude  ?>],
      zoom: 22,
      layers: [peta2]
    });

    var baseLayers = {
      "Strees": peta1,
      "Satelite": peta2,
    };
    L.control.layers(baseLayers).addTo(mymap);
    L.marker([<?= $perusahaan->latitude ?>, <?= $perusahaan->longitude ?>], {
      icon: L.icon({
        iconUrl: '<?= base_url('marker/' . $perusahaan->icon)  ?>',
        iconSize: [33, 44], // size of the icon
      })
    }).addTo(mymap).bindPopup("<img src='<?= base_url('gambar/' . $perusahaan->gambar) ?>' width='100%'>" +
      "Nama Tempat : <?= $perusahaan->nama_tempat ?></br>" +
      "Alamat : <?= $perusahaan->alamat ?></br>" +
      "Kabupaten : <?= $perusahaan->n_kabupaten ?></br>" +
      "<a href='https://www.google.com/maps/dir/?api=1&origin=" + "&destination=<?= $perusahaan->latitude ?>,<?= $perusahaan->longitude ?>' class='btn btn-sm btn-outline-success' target='_blank'>Rute</a>").openPopup();
</script>

