<div class="content-wrapper">
 <!-- Main content -->
  <section class="content-header">
      <h1>
        Edit Perusahaan
        <small>it all starts here</small>
      </h1>
    </section>
    <section class="content">
    	<div class="row">
<div class="col-lg-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
<div class="form-horizontal">
             <div class="box-body">
             <?php
				echo validation_errors('<div class="alert alert-warning alert-dismissible">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>', '</div>');
				if (isset($error_upload)) {
					echo '<div class="alert alert-danger alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' . $error_upload . '</div>';
				}


				echo form_open_multipart('perusahaan/edit/' . $perusahaan->id_perusahaan);
				?>
              
                <div class="form-group">
                	
                  <label  class="col-sm-2 control-label">Nama</label>

                  <div class="col-sm-4">
<input name="nama_tempat" value="<?= $perusahaan->nama_tempat ?>" placeholder="Nama Tempat" type="text" class="form-control">                  
</div>
                  <label  class="col-sm-1 control-label">Jenis Perusahaan</label>

                  <div class="col-sm-4">
                  <select name="id_icon" class="form-control">
            <option value="<?= $perusahaan->id_icon ?>"><?= $perusahaan->nama_icon ?></option>
              <?php foreach($icon as $row) { ?>
                <option value="<?php echo $row->id_icon;?>"><?php echo $row->nama_icon;?></option>
            <?php } ?>
          </select>
                  </div>
                </div> 
                <div class="form-group">
                  <label  class="col-sm-2 control-label">Alamat</label>

                  <div class="col-sm-9">
					        <input name="alamat" value="<?= $perusahaan->alamat ?>" placeholder="Alamat" class="form-control">
                  </div>
                </div>
                <div class="form-group">
                  <label  class="col-sm-2 control-label">No Telp</label>

                  <div class="col-sm-9">
					        <input type="number" name="telp" value="<?= $perusahaan->telp ?>" placeholder="Nomor Telepon" class="form-control" maxlength="13">
                  </div>
                </div>
                <div class="form-group">
                  <label  class="col-sm-2 control-label">Provinsi</label>

                  <div class="col-sm-4">
							<input name="prov" value="<?= $perusahaan->prov ?>" placeholder="Provinsi" class="form-control" readonly>
                  </div>
                  <label  class="col-sm-1 control-label"> Ubah Kabupaten</label>

                  <div class="col-sm-4">
                   <select class="form-control" name="id_kabupaten">
						<option value="<?= $perusahaan->id_kabupaten ?>"><?= $perusahaan->n_kabupaten ?></option>
            <?php foreach($kabupaten as $row) { ?>
                <option value="<?php echo $row->id_kabupaten;?>"><?php echo $row->n_kabupaten;?></option>
            <?php } ?>
        </select>    
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-2 control-label">Kabupaten</label>

                  <div class="col-sm-4">
							<input name="kabupaten" value="<?= $perusahaan->n_kabupaten ?>" placeholder="Kabupaten" type="text" class="form-control" readonly>
                  </div>
                  <!-- <label class="col-sm-1 control-label">Provinsi</label>

                  <div class="col-sm-4">
							<input name="prov" value="" placeholder="Provinsi" class="form-control" readonly>
                  </div> -->
                </div> 

                <div class="form-group">
                  <label class="col-sm-2 control-label">Latitude</label>

                  <div class="col-sm-4">
<input id="Latitude" value="<?= $perusahaan->latitude ?>" name="latitude" placeholder="Latitude" type="text" class="form-control">                  </div>
                  <label class="col-sm-1 control-label">Longitude</label>

                  <div class="col-sm-4">
<input name="longitude" value="<?= $perusahaan->longitude ?>" id="Longitude" placeholder="Longitude" class="form-control">                  </div>
                </div>   
                 <div class="form-group">
                <img src="<?= base_url('gambar/'.$perusahaan->gambar) ?>" width="120px">
                  <label class="col-sm-2 control-label">Gambar</label>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Ganti Gambar</label>

                  <div class="col-sm-9">
          <input type="file" name="gambar" class="form-control">
                  </div>
                </div>
                <div class="box-footer">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-info pull-right">Submit</button>
              </div>   
              <?php echo form_close(); ?>

              </div>
            </div>
              <!-- /.box-body -->
              
              <!-- /.box-footer -->

          </div>
      </div>          
  </section>
          <!-- /.box -->


</div>
<script>
  var curLocation = [0, 0];
  if (curLocation[0] == 0 && curLocation[1] == 0) {
    curLocation = [1.458558, 102.218316];
  }

  var mymap = L.map('mapid').setView([1.458558, 102.218316], 11);
  L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
      '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
      'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    id: 'mapbox/streets-v11'
  }).addTo(mymap);

  mymap.attributionControl.setPrefix(false);
  var marker = new L.marker(curLocation, {
    draggable: 'true'
  });

  marker.on('dragend', function(event) {
    var position = marker.getLatLng();
    marker.setLatLng(position, {
      draggable: 'true'
    }).bindPopup(position).update();
    $("#Latitude").val(position.lat);
    $("#Longitude").val(position.lng).keyup();
  });

  $("#Latitude, #Longitude").change(function() {
    var position = [parseInt($("#Latitude").val()), parseInt($("#Longitude").val())];
    marker.setLatLng(position, {
      draggable: 'true'
    }).bindPopup(position).update();
    mymap.panTo(position);
  });
  mymap.addLayer(marker);
</script>


  

