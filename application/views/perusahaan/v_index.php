<div class="content-wrapper">
       <!-- Main content -->
        <section class="content-header">
      <h1>
        <?php echo $title?>
        <small>it all starts here</small>
      </h1>
    </section>
    <section class="content">
<!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><?php echo $title?>
</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <?php

  if ($this->session->flashdata('pesan')) {
    echo '<div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
    echo $this->session->flashdata('pesan');
    echo '</div>';
  }

  ?>
        <div class="box-body table-responsive">
   <table id="example1" class="table table-hover">
                <thead>
                <tr>
                <th width="10px">No</th>
				<th>Nama Perusahaan</th>
				<th>Jenis Perusahaan</th>
				<th width="10%">Alamat</th>
				<th>Telepon</th>
				<th>Kabupaten</th>
        <th width="10px">Tumbnail</th>
				<th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $no = 1;
			foreach ($perusahaan as $key => $value) { ?>
                <tr>
                 <td><?= $no++; ?></td>
					<td><?= $value->nama_tempat ?></td>
					<td><?= $value->nama_icon ?></td>
					<td><?= $value->alamat ?></td>
					<td><?= $value->telp ?></td>
					<td><?= $value->n_kabupaten ?></td>
          <td width="10px"><img src="<?= base_url('gambar/' . $value->gambar)  ?>" width="75px" height="75px" class="img-circle"></td>
					<td><center>
						<a href="<?= base_url('perusahaan/edit/' . $value->id_perusahaan) ?>" class="btn btn-xs btn-success"><i class="fa fa-edit"></i> Edit</a>
						<a href="<?= base_url('perusahaan/delete/' . $value->id_perusahaan) ?>" onclick="return confirm('Apakah Data Ini Akan Dihapus..?')" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> Hapus</a>
						<a href="<?= base_url('home/detail/' . $value->id_perusahaan) ?>" class="btn btn-xs btn-success"><i class="fa fa-eye"></i> Detail</a></center>
					</td>
                </tr> 

            			<?php } ?>
			</tbody>
      </table>



         </div>
       
        <!-- /.box-footer-->

      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
