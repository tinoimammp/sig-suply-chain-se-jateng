<div class="content-wrapper">
 <!-- Main content -->
  <section class="content-header">
      <h1>
        <?php echo $title?>
        <small>it all starts here</small>
      </h1>
    </section>
    <section class="content">
    	<div class="row">
<div class="col-lg-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
<div class="form-horizontal">
             <div class="box-body">
              <?php
        echo validation_errors('<div class="alert alert-warning alert-dismissible">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>', '</div>');
        if (isset($error_upload)) {
          echo '<div class="alert alert-danger alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' . $error_upload . '</div>';
        }


        echo form_open_multipart('perusahaan/add');
        ?>
              
                <div class="form-group">
                	
                  <label  class="col-sm-2 control-label">Nama</label>

                  <div class="col-sm-10">
                    <input name="nama_tempat" type="text" class="form-control" placeholder="Nama Perusahaan" required>
                  </div>
                </div>
                <div class="form-group">
                  <label  class="col-sm-2 control-label">Alamat</label>

                  <div class="col-sm-10">
                    <input name="alamat" class="form-control" placeholder="Alamat Perusahaan" required>
                  </div>
                </div>
                <div class="form-group">
                  <label  class="col-sm-2 control-label">No Telepon</label>

                  <div class="col-sm-10">
                    <input type="number" name="telp" class="form-control" placeholder="Nomor Telepon" maxlength="15">
                  </div>
                </div>
                      <div class="form-group">
                  <label  class="col-sm-2 control-label">Email</label>

                  <div class="col-sm-10">
                    <input type="email" name="emailp" class="form-control" placeholder="E-Mail Perusahaan">
                  </div>
                </div>
                <div class="form-group">
                  <label  class="col-sm-2 control-label">Provinsi</label>

                  <div class="col-sm-3">
                    <input name="prov" class="form-control" placeholder="Jawa Tengah" value="Jawa Tengah" readonly>
                  </div>
                  <label  class="col-sm-1 control-label">Kabupaten</label>

                  <div class="col-sm-3">
                   <select class="form-control" name="id_kabupaten">
                <option  value="">---Pilih Kabupaten---</option>                    
            <?php foreach($kabupaten as $row) { ?>
                <option value="<?php echo $row->id_kabupaten;?>"><?php echo $row->n_kabupaten;?></option>
            <?php } ?>
        </select>    
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-2 control-label">Latitude</label>

                  <div class="col-sm-3">
                    <input id="Latitude" name="latitude" type="text" class="form-control" placeholder="" required>
                  </div>
                  <label class="col-sm-1 control-label">Longitude</label>

                  <div class="col-sm-3">
                    <input id="Longitude" name="longitude" class="form-control" placeholder="" required>
                  </div>
                </div>
                <div class="form-group">
                  <label  class="col-sm-2 control-label">Jenis Perusahaan</label>

                  <div class="col-sm-10">
                  <select name="id_icon" class="form-control" required>
            <?php foreach ($icon as $key => $value) { ?>
              <option value="<?= $value->id_icon ?>"><?= $value->nama_icon ?></option>
            <?php } ?>
          </select>
                  </div>
                </div>  
                <div class="form-group">
                  <label class="col-sm-2 control-label">Gambar</label>

                  <div class="col-sm-10">
          <input type="file" name="gambar" class="form-control" required>
                  </div>
                </div>
                <div class="box-footer">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-info pull-right">Submit</button>
              </div>   
              <?php echo form_close(); ?>

              </div>
            </div>
              <!-- /.box-body -->
              
              <!-- /.box-footer -->

          </div>
      </div>          
  </section>
          <!-- /.box -->


</div>
<script>
  var curLocation = [0, 0];
  if (curLocation[0] == 0 && curLocation[1] == 0) {
    curLocation = [1.458558, 102.218316];
  }

  var mymap = L.map('mapid').setView([1.458558, 102.218316], 11);
  L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
      '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
      'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    id: 'mapbox/streets-v11'
  }).addTo(mymap);

  mymap.attributionControl.setPrefix(false);
  var marker = new L.marker(curLocation, {
    draggable: 'true'
  });

  marker.on('dragend', function(event) {
    var position = marker.getLatLng();
    marker.setLatLng(position, {
      draggable: 'true'
    }).bindPopup(position).update();
    $("#Latitude").val(position.lat);
    $("#Longitude").val(position.lng).keyup();
  });

  $("#Latitude, #Longitude").change(function() {
    var position = [parseInt($("#Latitude").val()), parseInt($("#Longitude").val())];
    marker.setLatLng(position, {
      draggable: 'true'
    }).bindPopup(position).update();
    mymap.panTo(position);
  });
  mymap.addLayer(marker);
</script>


  
