	<div class="content-wrapper">
       <!-- Main content -->
    <section class="content">
<!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><?php echo $title?>
</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <?php

  if ($this->session->flashdata('pesan')) {
    echo '<div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
    echo $this->session->flashdata('pesan');
    echo '</div>';
  }

  ?>
        <div class="box-body table-responsive">
   <table id="example1" class="table table-hover">
                <thead>
                <tr>
                <th>No</th>
				<th>Nama Kategori</th>
				<th>Total Perusahaan</th>
				<th>Icon</th>
				<th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $no = 1;
			foreach ($hitung as $key => $value) { ?>

                <tr>
                 <td><?= $no++; ?></td>
					<td><?= $value->nama_icon ?></td>
					<td><?= $value->totalkat ?></td>
					<td><img src="<?= base_url('marker/' . $value->icon)  ?>" width="60px"></td>
					<td><a href="<?= base_url('icon/edit/' . $value->id_icon) ?>" class="btn btn-sm btn-success"><i class="fa fa-edit"></i> Edit</a>
						<a href="<?= base_url('icon/delete/' . $value->id_icon) ?>" onclick="return confirm('Apakah Data Ini Akan Dihapus..?')" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Hapus</a>
						<a href="<?= base_url('icon/detail/' . $value->id_icon) ?>" class="btn btn-sm btn-success"><i class="fa fa-eye"></i> Detail</a></td>
                </tr> 

            			<?php } ?>
			</tbody>
      </table>



         </div>
       
        <!-- /.box-footer-->

      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
    
  </div>
  <div>
</div>
  <!-- /.content-wrapper -->
