<div class="content-wrapper">
 <!-- Main content -->
  <section class="content-header">
      <h1>
        <?php echo $title?>
        <small>it all starts here</small>
      </h1>
    </section>
    <section class="content">
    	<div class="row">
<div class="col-md-6">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
<div class="form-horizontal">
	             <div class="box-body">
			<?php
			echo validation_errors('<div class="alert alert-warning alert-dismissible">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>', '</div>');
			if (isset($error_upload)) {
				echo '<div class="alert alert-danger alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' . $error_upload . '</div>';
			}


			echo form_open_multipart('icon/edit/' . $icon->id_icon);
			?>

			<div class="form-group">
                	
                  <label  class="col-sm-2 control-label">Jenis Perusahaan</label>

                  <div class="col-sm-10">
						<input name="nama_icon" value="<?= $icon->nama_icon ?>" placeholder="Nama Icon" class="form-control">
                  </div>
                </div>
                <div class="form-group">
                	
                  <label  class="col-sm-2 control-label">ICON</label>

                  <div class="col-sm-10">
				<img src="<?= base_url('marker/' . $icon->icon) ?>" width="60px">
                  </div>
                </div>
                <div class="form-group">
                	
                  <label  class="col-sm-2 control-label">Ganti ICON .PNG</label>

                  <div class="col-sm-10">
					<input type="file" name="marker" placeholder="Format .PNG" class="form-control">
                  </div>
                </div>

        <div class="box-footer">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-info pull-right">Submit</button>
              </div>   
              <?php echo form_close(); ?>

              </div>
            </div>
              <!-- /.box-body -->
              
              <!-- /.box-footer -->

          </div>
      </div>          
  </section>
          <!-- /.box -->


</div>
