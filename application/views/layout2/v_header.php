<body class="hold-transition skin-blue fixed  sidebar-mini skin-yellow">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?= base_url('admin') ?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>G</b>IS</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b></b>Pemetaan PT</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
           
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?= base_url('foto/' . $this->session->userdata('foto')) ?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?= $this->session->userdata('nama_user') ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?= base_url('foto/' . $this->session->userdata('foto')) ?>" class="img-circle" alt="User Image">

                <p>
              <?= $this->session->userdata('nama_user') ?>
                </p>
              </li>        
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?= base_url() ?>user" class="btn btn-default btn-flat">Pengaturan</a>
                </div>
                <div class="pull-right">
                  <a href="<?= base_url('auth/logout') ?>" class="btn btn-default btn-flat">Keluar</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?= base_url('foto/' . $this->session->userdata('foto')) ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p> <?= $this->session->userdata('nama_user') ?>
</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-location-arrow"></i> <span>Modul Perusahaan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?= base_url('perusahaan') ?>"><i class="fa fa-circle-o"></i> Data Perusahaan</small></a></li>
            <li><a href="<?= base_url('perusahaan/add') ?>"><i class="fa fa-circle-o"></i> Add Perusahaan</a></li>
          </ul>
        </li>
        <li><a href="<?= base_url('gallery') ?>"><i class="fa fa-book"></i> <span>Modul Foto Perusahaan</span></a></li>
          <li class="treeview">
          <a href="#">
            <i class="fa fa-map-marker"></i> <span>Modul Jenis Perusahaan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?= base_url('icon') ?>"><i class="fa fa-circle-o"></i>Data Jenis Perusahaan</a></li>
            <li><a href="<?= base_url('icon/add') ?>"><i class="fa fa-circle-o"></i>Add Jenis Perusahaan</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Modul Kabupaten</span>
             <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?= base_url('kabupaten') ?>"><i class="fa fa-circle-o"></i>Data Kabupaten</a></li>
            <li><a href="<?= base_url('kabupaten/add') ?>"><i class="fa fa-circle-o"></i>Add Kabupaten</a></li>
          </ul>
        </li>
        <!-- <li class="treeview">
          <a href="#">
            <i class="fa fa-pencil"></i>
            <span>Modul Berita</span>
             <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?= base_url('berita') ?>"><i class="fa fa-circle-o"></i>Data Berita</a></li>
            <li><a href="<?= base_url('berita/add') ?>"><i class="fa fa-circle-o"></i>Add Berita</a></li>
          </ul>
        </li> -->
<li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i>
            <span>Modul Administrator</span>
             <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?= base_url('user') ?>"><i class="fa fa-circle-o"></i>Data Administrator</a></li>
            <li><a href="<?= base_url('auth/logout') ?>"><i class="fa fa-circle-o"></i>Keluar</a></li>
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
  <!-- =============================================== -->