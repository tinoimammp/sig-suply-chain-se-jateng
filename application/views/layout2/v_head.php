<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Admin : <?= $title ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/bower_components/Ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/dist/css/skins/_all-skins.min.css">

	<!-- Jquery JS-->
	<!-- Bootstrap JS-->
	<!-- Leaflet-->
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <!-- LEAFLET -->
  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css" />
  <link rel="stylesheet" href="<?=base_url()?>geojson/leaflet-panel-layers-master/src/leaflet-panel-layers.css" />
  <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"></script>

    <link rel="stylesheet" href="<?= base_url() ?>geojson/leaflet-search.css" />
 <script src="<?= base_url() ?>geojson/leaflet.ajax.js"></script>
  <script src="<?= base_url() ?>geojson/leaflet.ajax.min.js"></script>
  <script src="<?= base_url() ?>geojson/leaflet-search.js"></script>
  <script src="<?=base_url('geojson/node_modules/leaflet-easyprint/dist/bundle.js')?>"></script>
  <link rel="stylesheet" href="<?=base_url('geojson/leaflet-compass-master/src/leaflet-compass.css')?>" />
  <script src="<?=base_url('geojson/leaflet-panel-layers-master/src/leaflet-panel-layers.js')?>"></script>
  <script src="<?=base_url('geojson/Leaflet.GoogleMutant.js')?>"></script>
  <script src="<?=base_url('geojson/node_modules/leaflet-easyprint/dist/bundle.js')?>"></script>
  <script src="<?=base_url('geojson/leaflet-search/dist/leaflet-search.src.js')?>"></script>
  <script src="<?=base_url('api/data/kecamatan')?>"></script>
  <script src="<?=base_url('api/data/perusahaan/varpoint')?>"></script>
  <script src="<?=base_url('api/data/icon')?>"></script>
</head>