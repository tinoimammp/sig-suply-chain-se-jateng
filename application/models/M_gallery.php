<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_gallery extends CI_Model {

	public function lists()
	{
		$this->db->select('tbl_perusahaan.*,COUNT(tbl_foto.id_perusahaan) as total_foto');
		$this->db->from('tbl_perusahaan');
		$this->db->join('tbl_foto', 'tbl_foto.id_perusahaan = tbl_perusahaan.id_perusahaan', 'left');
		$this->db->group_by('tbl_perusahaan.id_perusahaan');
		$this->db->order_by('id_perusahaan','desc');
		$query=$this->db->get();
		return $query->result();
	}	

	public function detail($id_perusahaan)
	{
		$this->db->select('*');
		$this->db->from('tbl_perusahaan');
		$this->db->where('id_perusahaan', $id_perusahaan);
		$query=$this->db->get();
		return $query->row();
	}

	public function detailfoto($id_foto)
	{
		$this->db->select('*');
		$this->db->from('tbl_foto');
		$this->db->where('id_foto', $id_foto);
		$query=$this->db->get();
		return $query->row();
	}

	public function foto($id_perusahaan)
	{
		$this->db->select('*');
		$this->db->from('tbl_foto');
		$this->db->where('id_perusahaan', $id_perusahaan);
		$this->db->order_by('id_foto', 'desc');
		$query=$this->db->get();
		return $query->result();

	}

	public function addfoto($data)
	{
		$this->db->insert('tbl_foto', $data);
	}

	//hapus data kategori
	public function delete($data)
	{
		$this->db->where('id_foto', $data['id_foto']);
		$this->db->delete('tbl_foto',$data);
	}


}

/* End of file M_gallery.php */
/* Location: ./application/models/M_gallery.php */