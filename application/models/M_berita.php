<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_berita extends CI_Model {

	public function lists()
	{
		$this->db->select('*');
		$this->db->from('tbl_berita');
		$this->db->order_by('id_berita','DESC');
		return $this->db->get()->result();
	}

	public function detail($id_berita)
	{
		$this->db->select('*');
		$this->db->from('tbl_berita');
		$this->db->where('id_berita', $id_berita);
		return $this->db->get()->row();
	}

	public function add($data)
	{
		$this->db->insert('tbl_berita', $data);
		
	}

	public function edit($data)
	{
		$this->db->where('id_berita', $data['id_berita']);
		$this->db->update('tbl_berita',$data);	
	}

	public function delete($data)
	{
		$this->db->where('id_berita', $data['id_berita']);
		$this->db->delete('tbl_berita',$data);	
	}

		//memunculkan berita deg paging
	public function berita($limit,$start)
	{
		$this->db->select('*');
		$this->db->from('tbl_berita');
	
		$this->db->order_by('id_berita', 'desc');
		$this->db->limit($limit,$start);
		return $this->db->get()->result();		
	}
public function detail_berita($slug_berita)
	{
		$this->db->select('*');
		$this->db->from('tbl_berita');
	
		$this->db->where('slug_berita', $slug_berita);		
		return $this->db->get()->row();		
	}

public function latest_berita()
	{
		$this->db->select('*');
		$this->db->from('tbl_berita');
	
		$this->db->order_by('tgl_berita', 'desc');
		$this->db->limit(3);
		return $this->db->get()->result();	
	}
	public function beritaku()
	{
		$this->db->select('*');
		$this->db->from('tbl_berita');
	
		$this->db->order_by('id_berita', 'desc');
		$this->db->limit(7);
		return $this->db->get()->result();	
	}
	
	public function total_berita()
	{
		$this->db->select('*');
		$this->db->from('tbl_berita');
		$this->db->order_by('id_berita', 'desc');
		return $this->db->get()->result();
	}
		public function totalberita()
{
    $query = $this->db->query('SELECT * FROM tbl_berita WHERE id_berita');
 $totalberita=$query->num_rows();
 return $totalberita;
}
}

/* End of file M_berita.php */
