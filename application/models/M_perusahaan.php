<?php

defined('BASEPATH') or exit('No direct script access allowed');

class M_perusahaan extends CI_Model
{

	public function get_all_data()
	{
		$this->db->select('*');
		$this->db->from('tbl_perusahaan');
		$this->db->join('tbl_kabupaten', 'tbl_kabupaten.id_kabupaten= tbl_perusahaan.id_kabupaten');
		$this->db->join('tbl_icon', 'tbl_icon.id_icon = tbl_perusahaan.id_icon', 'left');
		$this->db->order_by('id_perusahaan', 'ASC');
		return $this->db->get()->result();
	}


	public function add($data)
	{
		$this->db->insert('tbl_perusahaan', $data);
	}

public function pemetaan($data)
	{
		$this->db->select('*');
		$this->db->from('tbl_perusahaan');
		$this->db->join('tbl_icon', 'tbl_icon.id_icon = tbl_perusahaan.id_icon', 'left');
		$this->db->join('tbl_kabupaten', 'tbl_kabupaten.id_kabupaten = tbl_perusahaan.id_kabupaten', 'left');
		$this->db->where('tbl_perusahaan.id_icon', $data);
		return $this->db->get()->result();
	}
	public function detail($id_perusahaan)
	{
		
		$this->db->select('*');
		$this->db->from('tbl_perusahaan');
		$this->db->join('tbl_kabupaten', 'tbl_kabupaten.id_kabupaten = tbl_perusahaan.id_kabupaten');

		$this->db->join('tbl_icon', 'tbl_icon.id_icon = tbl_perusahaan.id_icon', 'left');

		$this->db->where('id_perusahaan', $id_perusahaan);
		return $this->db->get()->row();
	}

	public function edit($data)
	{
		$this->db->where('id_perusahaan', $data['id_perusahaan']);
		$this->db->join('tbl_kabupaten', 'tbl_kabupaten.id_kabupaten = tbl_perusahaan.id_kabupaten');
		$this->db->update('tbl_perusahaan', $data);
	}

	public function delete($data)
	{
		$this->db->where('id_perusahaan', $data['id_perusahaan']);
		$this->db->delete('tbl_perusahaan', $data);
	}

		public function total()
{
    $query = $this->db->query('SELECT * FROM tbl_perusahaan WHERE id_perusahaan');
 $total=$query->num_rows();
 return $total;
}

	public function fotod($id_perusahaan)
	{
		$this->db->select('*');
		$this->db->from('tbl_foto');
		$this->db->where('id_perusahaan', $id_perusahaan);
		$query=$this->db->get();
		return $query->result();

	}

	function get(){
		$data=$this->db->select('*')
					->from('tbl_perusahaan a')
					->join('tbl_kabupaten b','a.id_kabupaten=b.id_kabupaten','LEFT')
					->join('tbl_icon c','a.id_icon=c.id_icon','LEFT')
					->get();
		return $data;
	}
}

/* End of file M_perusahaan.php */
