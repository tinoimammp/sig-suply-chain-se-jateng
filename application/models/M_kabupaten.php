<?php

defined('BASEPATH') or exit('No direct script access allowed');

class M_kabupaten extends CI_Model
{

	public function get_all_data()
	{
		$this->db->select('*');
		$this->db->from('tbl_kabupaten');
		$this->db->order_by('id_kabupaten', 'ASC');
		return $this->db->get()->result();
	}

		public function hitung()
	{
		$this->db->select('tbl_kabupaten.*,COUNT(tbl_perusahaan.id_kabupaten) as totalkabupaten');
		$this->db->from('tbl_kabupaten');
		$this->db->join('tbl_perusahaan', 'tbl_kabupaten.id_kabupaten = tbl_perusahaan.id_kabupaten', 'left');
		$this->db->group_by('tbl_kabupaten.id_kabupaten');
		$this->db->order_by('id_kabupaten','desc');
		$query=$this->db->get();
		return $query->result();
	}

	function get(){
		$data=$this->db->get('tbl_kabupaten');
		return $data;
	}

	//public function hitung()
	//{
	//	$this->db->select('*');
	//	$this->db->from('tbl_kabupaten',' id_kabupaten, COUNT(id_perusahaan) as total');
//$this->db->join('tbl_perusahaan', 'tbl_perusahaan.id_perusahaan = tbl_perusahaan.id_perusahaan','left');
	//	$this->db->group_by('id_kabupaten');
//$this->db->order_by('id_perusahaan', 'desc');
	//	echo $this->db->count_all_results();
	//	return $this->db->get()->result();
	//}


	public function add($data)
	{
		$this->db->insert('tbl_kabupaten', $data);
	}

	public function detaixl($id_kabupaten)
	{
		$this->db->select('*');
		$this->db->from('tbl_kabupaten');
		$this->db->where('id_kabupaten', $id_kabupaten);
		return $this->db->get()->row();
	}


	public function detail($data)
	{
		$this->db->select('*');
		$this->db->from('tbl_perusahaan');
		$this->db->join('tbl_icon', 'tbl_icon.id_icon = tbl_perusahaan.id_icon', 'left');
		$this->db->join('tbl_kabupaten', 'tbl_kabupaten.id_kabupaten = tbl_perusahaan.id_kabupaten', 'left');
		$this->db->where('tbl_perusahaan.id_kabupaten', $data);
		return $this->db->get()->result();
	}

public function perusahaankabupaten($data)
	{
		$this->db->select('*');
		$this->db->from('tbl_perusahaan');
		$this->db->join('tbl_icon', 'tbl_icon.id_icon = tbl_perusahaan.id_icon', 'left');
		$this->db->join('tbl_kabupaten', 'tbl_kabupaten.id_kabupaten = tbl_perusahaan.id_kabupaten', 'left');
		$this->db->where('tbl_perusahaan.id_kabupaten', $data);
		return $this->db->get()->result();
	}

	public function edit($data)
	{
		$this->db->where('id_kabupaten', $data['id_kabupaten']);
		$this->db->update('tbl_kabupaten', $data);

	}

	public function delete($data)
	{
		$this->db->where('id_kabupaten', $data['id_kabupaten']);
		$this->db->delete('tbl_kabupaten', $data);
	}

	public function totalkabupaten()
{
    $query = $this->db->query('SELECT * FROM tbl_kabupaten WHERE id_kabupaten');
 $totalkabupaten=$query->num_rows();
 return $totalkabupaten;
}
}

/* End of file M_user.php */
