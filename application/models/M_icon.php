<?php

defined('BASEPATH') or exit('No direct script access allowed');

class M_icon extends CI_Model
{

	public function get_all_data()
	{
		$this->db->select('*');
		$this->db->from('tbl_icon');
		$this->db->order_by('id_icon', 'desc');
		
		return $this->db->get()->result();
	}

	public function hitung()
	{
		$this->db->select('tbl_icon.*,COUNT(tbl_perusahaan.id_icon) as totalkat');
		$this->db->from('tbl_icon');
		$this->db->join('tbl_perusahaan', 'tbl_icon.id_icon = tbl_perusahaan.id_icon', 'left');
		$this->db->group_by('tbl_icon.id_icon');
		$this->db->order_by('id_icon','desc');
		$query=$this->db->get();
		return $query->result();
	}

	public function add($data)
	{
		$this->db->insert('tbl_icon', $data);
	}

	public function detailx($id_icon)
	{
		$this->db->select('*');
		$this->db->from('tbl_icon');
		$this->db->where('id_icon', $id_icon);
		return $this->db->get()->row();
	}

	public function detail($data)
	{
		$this->db->select('*');
		$this->db->from('tbl_perusahaan');
		$this->db->join('tbl_icon', 'tbl_icon.id_icon = tbl_perusahaan.id_icon', 'left');
		$this->db->join('tbl_kabupaten', 'tbl_kabupaten.id_kabupaten = tbl_perusahaan.id_kabupaten', 'left');
		$this->db->where('tbl_perusahaan.id_icon', $data);
		return $this->db->get()->result();
	}
	

	public function edit($data)
	{
		$this->db->where('id_icon', $data['id_icon']);
		$this->db->update('tbl_icon', $data);
	}

	public function delete($data)
	{

		$this->db->where('id_icon', $data['id_icon']);
		$this->db->delete('tbl_icon', $data);
	}

	public function totalkategori()
{
    $query = $this->db->query('SELECT * FROM tbl_icon WHERE id_icon');
 $totalkategori=$query->num_rows();
 return $totalkategori;
}

function get(){
		$data=$this->db->get('tbl_icon');
		return $data;
	}
}

/* End of file M_icon.php */
